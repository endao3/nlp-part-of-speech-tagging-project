package ML_Algorithms;

/**
 * Porter Stemmer from Apache Lucene-Snowball
 */

import org.tartarus.snowball.SnowballStemmer;
import org.tartarus.snowball.ext.*;

public class Stemmer {

    private SnowballStemmer stemmer;
    private String dialect;

    public Stemmer(String language) {
        stemmer = setStemmerLanguage(language);
    }

    /**
     * Get stem of word
     * @param word
     * @return
     */
    public String getStem(String word){
        stemmer.setCurrent(word); //set string you need to stem
        stemmer.stem();  //stem the word
        return(stemmer.getCurrent());//get the stemmed word
    }

    /**
     * Set Stemmer Type based on Language
     * @param language
     * @return
     */
    private SnowballStemmer setStemmerLanguage(String language){
        switch (language){
            case "en":
                this.dialect = "en";
                return new englishStemmer();//English
            case "nl":
                this.dialect = "nl";
                return new dutchStemmer();//Dutch
            case "fr":
                this.dialect = "fr";
                return new frenchStemmer();//French
            case "de":
                this.dialect = "de";
                return new germanStemmer();//German
            case "ga":
                this.dialect = "ga";
                return new irishStemmer();//Irish
            case "it":
                this.dialect = "it";
                return new italianStemmer();//Italian
            case "es":
                this.dialect = "es";
                return new spanishStemmer();//Spanish
            case "pt":
                this.dialect = "pt";
                return new portugueseStemmer();
            case "ru":
                this.dialect = "ru";
                return new russianStemmer();
        }
        return null;
    }

    public String getDialect() {
        return dialect;
    }
}
