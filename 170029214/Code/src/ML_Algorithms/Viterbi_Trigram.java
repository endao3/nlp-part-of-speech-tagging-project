package ML_Algorithms;

import HMM.Node;
import HMM.Tag;
import HMM.Word;
import HMM.Word_Morphology;
import Probabilities.Transition;

import java.util.*;

public class Viterbi_Trigram {
/*
    //Current Tag Parameters
    private boolean starterTag, stopperTag, tagEmissionFound = false;

    //Accuracy Parameters
    private int totalTags = 0, sequenceTags = 0, wordEmissionFrequency;
    private double perplexity = 0;

    //Lists of Training-Data Parameters
    private List<Tag> listOfTags;
    private List<Word> listOfWords;
    private List<Node> listOfNodes;
    private List<String> listOfSentences;

    //Good-Turing Parameters
    private Map<String,List<SmoothedProbability>> tbl_GTValues;
    List<SmoothedProbability> lst_SmoothedValues;

    //Morphology Parameters
    private Stemmer stemmer;
    private Normalizer normalizer;
    private Word_Morphology morphology;
    private List<Double> transitionPerplexities;

    //Viterbi Parameters
    private List<Transition> newViterbiValues;
    private List<List<Transition>> transitionsTable;
    private List<Transition> previousViterbiValues = new ArrayList<>();

    private static final Viterbi_Trigram viterbi_Trigram = new Viterbi_Trigram();
    public Viterbi_Trigram() { }
    //Singleton - Algorithm never changes
    public static Viterbi_Trigram getInstance(){ return viterbi_Trigram; }

    /**
     * SETUP Viterbi Parameters
     * @param listOfSentences
     * @param listOfWords
     * @param listOfNodes
     * @param listOfTags
     * @param stemmer
     *
    public void setupViterbi(List<String> listOfSentences, List<Word> listOfWords, List<Node> listOfNodes,
                             List<Tag> listOfTags, Map<String,List<SmoothedProbability>>  tbl_GTValues, Stemmer stemmer ){

        this.listOfSentences = listOfSentences;
        this.listOfNodes = listOfNodes;
        this.listOfWords = listOfWords;
        this.tbl_GTValues = tbl_GTValues;

        this.listOfTags = listOfTags;
        normalizer = new Normalizer(listOfTags);

        this.stemmer = stemmer;
    }


    /**
     * --------------------------------------
     * SENTENCE FORMATTING
     * --------------------------------------
     */

    /**
     * Get tag sequence
     *
    public void getTagSequence()throws Exception
    {
        String correctTag;
        transitionPerplexities = new ArrayList<>();

        //Current sentence
        List<String> correctSequence = new ArrayList<>();
        List<String> predictedSequence = new ArrayList<>();
        List<String> textSentence = new ArrayList<>();
        transitionsTable = new ArrayList<>();
        Transition maxTransition;

        String words [];

        for(String line: listOfSentences) {

            if (!line.startsWith("#") && !line.isEmpty()) {
                words = line.split("\t");
                newViterbiValues = new ArrayList<>();

                //Keep track of sentence currently being processed
                correctTag = words[3];
                if(!words[2].matches("[^_=+@#\\\\|\\[{\\]}~&()*'\"<>/]*")) {
                    correctTag = listOfTags.get(listOfTags.size() - 2).getValue(); //Set to UNK Tag
                }
                correctSequence.add(correctTag);
                textSentence.add(words[1]);
                tagEmissionFound = determineEmissionValueFound(words[2]);

                //Get first <START> transition probabilities
                if(words[0].equals("1")) {
                    starterTag = true;
                    setInitialVBValue(listOfNodes.get(0), words[2]);
                    transitionsTable.add(newViterbiValues); //Add new list of Viterbi Values
                    previousViterbiValues = newViterbiValues; //Keep pointers to previous Viterbi Values
                }

                //Get second <START> transition probabilities
                else if(words[0].equals("2")) {
                    starterTag = true;
                    setViterbiValues(listOfNodes.get(0), words[2]);
                    transitionsTable.add(newViterbiValues); //Add new list of Viterbi Values
                    previousViterbiValues = newViterbiValues; //Keep pointers to previous Viterbi Values
                }

                //Get all node transitions
                else{
                    starterTag = false;
                    for(Node node1: listOfNodes) {
                        if (!node1.getTagValue().equals("<STOP>") && !node1.getTagValue().equals("<START>"))
                            setViterbiValues(node1, words[2]);
                    }
                    transitionsTable.add(newViterbiValues);
                    previousViterbiValues = newViterbiValues;
                }

                maxTransition = Collections.min(newViterbiValues, Comparator.comparing(tr -> tr.getTransition_Probability()));
                predictedSequence.add(maxTransition.getSecondNode().getTagValue());

                //if(!starterTag)
                //currentTransitionProbability = currentTransitionList.get(maxTransition.getSecondNode().getTagValue());

                //transitionPerplexities.add(currentTransitionProbability);

            }
            else{//Calculate accuracy and Reset for next sequence
                if(line.isEmpty()) {
                    //determineSentenceAccuracy(correctSequence, predictedSequence, textSentence);
                    predictedSequence = new ArrayList<>();
                    correctSequence = new ArrayList<>();
                    textSentence = new ArrayList<>();
                    break;
                }
            }

        }
    }


    /**
     * --------------------------------------
     * VITERBI ALGORITHM
     * --------------------------------------
     */


    /**
     * Add updated first table
     * @param startNode
     * @param word
     *
    public void setInitialVBValue(Node startNode, String word)
    {
        double currentEmissionMax = 0;
        double currentTransitionMax = 0;
        double logCurrentVal, logTransVal;
        String transitionTagValue;

        //Require another loop?? TAG|<START>, <START>
        for (Transition tr : startNode.getTrigramTransitions()) {

            if(tr.getFirstNode().getTagValue().equals("<START>")) { //ELSE != <START> for second word in sentence
                transitionTagValue = tr.getSecondNode().getTagValue();

                /**
                 * EMISSION VALUE
                 *
                if(!tagEmissionFound) {
                    //boolean affixFound = normalizer.existsAffix(morphology.getSuffix());

                    morphology = new Word_Morphology(stemmer.getStem(word);//word), word);
                    if(morphology.isDigit() && transitionTagValue.equals("NUM")) //NUM Tag
                        currentEmissionMax = 0.9;
                    else {
                        if(morphology.getSuffix()!=null)// && affixFound)//Not NUM and not Capital
                            currentEmissionMax = normalizer.getNormalized_AffixValue(tr.getFirstNode().getState().getTag(), false);
                        //currentEmissionMax = tr.getSecondNode().getState().getTag().checkAffixes(morphology.getSuffix());
                    }
                }
                else
                    currentEmissionMax = getWordFrequency(word, transitionTagValue);

                if(currentEmissionMax == 0)
                    currentEmissionMax = 0.0000001;
                //Will get Overflow when multiplying: Use -log addition
                logCurrentVal = (-1 * (Math.log(currentEmissionMax)));

                /**
                 * TRANSITION VALUE
                 *
                currentTransitionMax = tr.getTransition_Probability();
                if(currentTransitionMax == 0 || Double.isNaN(currentTransitionMax))
                    currentTransitionMax = 0.0000001;
                logTransVal = (-1*(Math.log(currentTransitionMax)));///Math.log(2)));

                /**
                 * FINAL CALCULATION
                 *
                logCurrentVal = Math.exp(logCurrentVal + logTransVal);

                //currentTransitionProbability = (-1*(Math.log((tr.getTransition_Probability()*currentEmissionMax))/Math.log(2)));
                newViterbiValues.add(new Transition(tr.getFirstNode(), tr.getSecondNode(), startNode, (double) logCurrentVal));
            }
        }
    }



    /**
     * Add updated first table
     * @param givenNode
     * @param word
     *
    public void setViterbiValues(Node givenNode, String word)
    {
        boolean viterbiFound, valueSet = false;
        Transition logMaxTransitionValue = null;
        String transitionTagValue;
        double viterbiLogBaseTwo = 0;

        double currentEmissionMax = 0;
        double currentTransitionMax = 0;
        double currentViterbiMax = 0;
        double logMax, logCurrentMax, logTransVal, logViterbi;

        /**
         * ADJ|<START>,ADJ
         * CCONJ|ADJ,NOUN:
         *
        for(Node firstNode: listOfNodes) {
            //First Tag if not START/STOP
            valueSet = false;
            logMax = Double.MAX_VALUE; logCurrentMax = 0; logTransVal = 0; logViterbi=0;

            if(!firstNode.getTagValue().equals("<START>") && !firstNode.getTagValue().equals("<STOP>")) {
                for (Transition tr : givenNode.getTrigramTransitions()) {
                    //First tag matches Current Node and no START or STOP Tag for Node we want emission for.
                    if (tr.getFirstNode().getTagValue().equals(firstNode.getTagValue())
                            && !tr.getSecondNode().getTagValue().equals("<START>")
                            && !tr.getSecondNode().getTagValue().equals("<STOP>")) {

                        transitionTagValue = tr.getSecondNode().getTagValue();

                        /**
                         * EMISSION VALUE
                         *
                        if(!tagEmissionFound) {
                            boolean affixFound = normalizer.existsAffix(morphology.getSuffix());

                            morphology = new Word_Morphology(stemmer.getStem(word), word);
                            if(morphology.isDigit()) //NUM Tag
                            {
                                if (transitionTagValue.equals("NUM"))
                                    currentEmissionMax = 0.9;
                                else
                                    currentEmissionMax = 0.000001;
                            }

                            else if(morphology.isCapital() && !starterTag)//PROPN Tag
                            {
                                if (transitionTagValue.equals("PROPN"))
                                    currentEmissionMax = 0.9;
                                else
                                    currentEmissionMax = 0.00001;
                            }
                            else {
                                if(morphology.getSuffix()!=null && affixFound) //Not NUM and not Capitalized
                                    currentEmissionMax = normalizer.getNormalized_AffixValue(tr.getFirstNode().getState().getTag());
                            }
                        }
                        else
                            currentEmissionMax = getWordFrequency(word, transitionTagValue);

                        if(currentEmissionMax == 0)
                            currentEmissionMax = 0.0000001;
                        logCurrentMax = (-1*(Math.log(currentEmissionMax)));

                        /**
                         * TRANSITION VALUE
                         *
                        currentTransitionMax = tr.getTransition_Probability();
                        if(currentTransitionMax == 0 || Double.isNaN(currentTransitionMax))
                            currentTransitionMax = 0.0000001;
                        logTransVal = (-1*(Math.log(currentTransitionMax)));///Math.log(2)));

                        /**
                         * VITERBI VALUE

                         //Max Viterbi found for current node, do not add new Viterbi if you break from loop
                         viterbiFound = false;
                         while (!viterbiFound) { //Exit once previous associative Viterbi value found
                         for (Transition vtrbs : previousViterbiValues) {
                         //First Node from <START> goes to should correspond to FirstNode of previous Viterbi Values (already had tag..)
                         if(!starterTag) {
                         if (tr.getFirstNode().getTagValue().equals(vtrbs.getFirstNode().getTagValue())) {
                         currentViterbiMax = vtrbs.getTransition_Probability();
                         if(currentViterbiMax == 0)
                         currentViterbiMax = 0.0000001;

                         logViterbi = (-1*(Math.log( currentViterbiMax)));///Math.log(2)));
                         viterbiLogBaseTwo =  currentViterbiMax;
                         viterbiFound = true;
                         break;
                         }
                         }
                         else{
                         if (tr.getFirstNode().getTagValue().equals(vtrbs.getSecondNode().getTagValue())) {
                         currentViterbiMax = vtrbs.getTransition_Probability();
                         if(currentViterbiMax == 0)
                         currentViterbiMax = 0.0000001;

                         logViterbi = (-1*(Math.log( currentViterbiMax)));///Math.log(2)));
                         viterbiLogBaseTwo =  currentViterbiMax;
                         viterbiFound = true;
                         break;
                         }
                         }
                         }
                         }*/

                        /**
                         * FINAL CALCULATION
                         *
                        logCurrentMax = Math.exp(logCurrentMax + logTransVal);// + logViterbi);
                        valueSet = true; //Loop entered with valid Viterbi calculated

                        if (logCurrentMax < logMax) {
                            //currentTransitionProbability = (-1*(Math.log(tr.getTransition_Probability()*currentEmissionMax*viterbiLogBaseTwo)/Math.log(2)));
                            logMax = logCurrentMax;
                            logMaxTransitionValue = new Transition(tr.getFirstNode(), tr.getSecondNode(), givenNode, (double) logMax);
                        }

                    }
                }
            }
            if(valueSet) {
                newViterbiValues.add(logMaxTransitionValue);
                //currentTransitionList.put(logMaxTransitionValue.getSecondNode().getTagValue(), currentTransitionProbability);
            }
        }
    }


    /**
     * --------------------------------------
     * EMISSIONS
     * --------------------------------------
     */


    /**
     * Determine if emissions value exists
     * @param word
     * @return
     *
    private boolean determineEmissionValueFound(String word)
    {
        for (Tag tag: listOfTags) {
            if(!tag.getValue().equals("<START>") && !tag.getValue().equals("<STOP>")) {
                if (getWordFrequency(word, tag.getValue()) != 0)
                    return true;
            }
        }
        return false;
    }

    /**
     * Get Emission Probability
     * @param textWord
     * @return
     *
    public int getWordFrequency(String textWord, String tag){

        for(Word word: listOfWords) {
            if (textWord.equals(word.getValue()) && (tag.equals(word.getTag().getValue()))) {
                return word.getFrequency();
            }
        }

        return 0;
    }


    /**
     * --------------------------------------
     * ACCURACY CALCULATIONS
     * --------------------------------------
     */

    /**
     * Get overall accuracy

    public void calculateAccuracy()
    {
        /*
        System.out.println("\nTag Ratios\n"+
                "***********\n"
                + predictedTags + ":" + totalTags);

        //Cross-Entropy - Trigram Language Model
        perplexity *= (double)1/totalTags;
        System.out.println("Perplexity of Model: "+Math.pow(2, (perplexity))+"\n\n");

        System.out.println("Sequence Accuracy: " + ((double)sequenceTags/totalTags) +"\n");
    }

    /**
     * Retrieve most Probable Sequence from BackPointer table
     * Compare Probable Sequence vs Actual Sequence
     *
     * @param correctSequence
     *
    private void sequenceAccuracy(List<String> correctSequence){

        String sequenceArray[] = new String[correctSequence.size()];

        //Retrieve most probable sequence from BackPointers
        for(int i=1; i<transitionsTable.size();i++) {
            Transition tran = Collections.min(transitionsTable.get(i), Comparator.comparing(tr -> tr.getTransition_Probability()));
            sequenceArray[i-1] = tran.getSecondNode().getTagValue();

            if (i == transitionsTable.size()-1)
                sequenceArray[i-1] = tran.getFirstNode().getTagValue();
        }

        //Compare Probable Sequence vs Actual Sequence
        for(int j=0;j<sequenceArray.length;j++) {
            totalTags++;
            if (sequenceArray[j].equals(correctSequence.get(j))){
                //System.out.println(sequenceArray[j]);
                sequenceTags++;
            }
            //else
            //System.out.println("Index:"+j+" -> CORRECT:"+ correctSequence.get(j)+"; WRONG:"+sequenceArray[j]);
        }
    }*/
}
