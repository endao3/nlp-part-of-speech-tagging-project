package ML_Algorithms;

import HMM.Tag;
import HMM.Word;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * UNUSED MACHINE LEARNING TECHNIQUE
 */
public class DecisionTree {

    private List<Word> listOfWords;
    private List<Tag> listOfTags;
    private Map<Tag, Double> informationGain;

    /**
     * Constructor: initialise J48 Tree
     */
    public DecisionTree(List<Tag> listOfTags, List<Word> listOfWords) {
        this.listOfTags = listOfTags;
        this.listOfWords = listOfWords;
        //calculateRootPurity();
    }

    /**
     * Node purity for each tag
     * ------------------------
     * Sum
     * -(C'(Tag) / C'(Total Tag) log2 -(C'(Tag) / C'(Total Tag))
     * -(C'(~Tag)/C'(Total Tag)) log2 -(C'(~Tag)/C'(Total Tag))
     */
    public void calculateRootPurity(){

        int totalTags = calculateTotalTags();
        informationGain = new HashMap<>();
        double tagValue, logValue, otherTagValues;

        for(Tag tag: listOfTags){
            if(!tag.getValue().contains("<START>") && !tag.getValue().contains("<STOP>")) {
                tagValue = -1 * ((double) tag.getFrequency()) / totalTags;
                logValue = Math.log(-1*tagValue) / Math.log(2);
                tagValue *= logValue;

                otherTagValues = -1 * ((double) totalTags - tag.getFrequency()) / totalTags;
                logValue = Math.log(-1*otherTagValues) / Math.log(2);
                otherTagValues *= logValue;

                informationGain.put(tag, (tagValue + otherTagValues));
            }
        }
    }


    private int calculateTotalTags(){
        int totalTags = 0;

        for(int i=0;i<listOfTags.size();i++)
        {
            if(!listOfTags.get(i).getValue().contains("<START>") && !listOfTags.get(i).getValue().contains("<STOP>"))
                totalTags+=listOfTags.get(i).getFrequency();
        }

        return totalTags;
    }


}
