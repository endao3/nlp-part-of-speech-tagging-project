package ML_Algorithms;
//TODO Interface
/*
    TODO
    1. Input: Sentence - X1,...,xn
              Parameters - q(s|u,v) e(X|s)

    2. Initialisation: Set π(0,START,START) = 1

    3. Definition: S-1 == S0 = {START}, Sk = S for k∈{1..n}

    4. Recursive Algorithm: For k=1...n and u ∈ Sk-1, v ∈ Sk

                  * Look for tag that maximises sequence ending in u,v at index k
                      π(k,u,v) = max(π(k-1,w,u) q(v|w,u) e(Xk|v)
                                w ∈ Sk-2

                  * Record which tag achieved max value at Sk-2 (BackPointers)
                      bp(k,u,v) = arg max(π(k-1,w,u) q(v|w,u) e(Xk|v))
                                  w ∈ Sk-2

                  * Last two tags in sequence, account for STOP tag
                      Set (Yn-1, Yn) = arg max (π(n,u,v) q(STOP|u,v)
                                        (u,v)

                  * Go backwards getting max tag value at each point to form complete sequence
                      For k=(n-2)...1, Yk = bp(k+2, Yk+1, Yk+2)

                  * Return the tag sequence
                      Y1...Yn

     5. Smoothing for every variable (Remove zero and low probabilities)
     */

import HMM.*;
import Probabilities.Transition;
import Smoothing.Normalizer;
import Smoothing.SmoothedProbability;

import java.lang.*;

import java.util.*;

public class Viterbi_Bigram {

    private boolean EOF;

    //Current Tag Parameters
    private boolean starterTag, stopperTag, tagEmissionFound, GoodTuringSmoothing;
    List<String> correctSequence;
    List<String> textSentence;

    //Accuracy Parameters
    private List<String> lst_UnknownWords;
    private int totalTags_TestingSet, sequenceTags, wordEmissionFrequency;
    private int totalWords_TestingSet, num_IncorrectlyTaggedUnknownWords, num_UnknownWords, num_IncorrectlyTaggedKnownWords;
    private double perplexity;
    int total_WordLength;

    //Lists of Training-Data Parameters
    private List<Tag> listOfTags;
    private List<Word> listOfWords;
    private List<Node> listOfNodes;
    private List<String> listOfSentences;

    //Good-Turing Parameters
    private Map<String,List<SmoothedProbability>> tbl_GTValues;
    private Map<String,List<SmoothedProbability>> tbl_IPDValues;
    List<SmoothedProbability> lst_SmoothedValues;

    //Morphology Parameters
    private DecisionTree decisionTree;
    private boolean prefixFound, suffixFound;
    private Stemmer stemmer;
    private Normalizer normalizer;
    private Word_Morphology morphology;

    //Viterbi Parameters
    private List<Transition> newViterbiValues;
    private List<List<Transition>> transitionsTable;
    private List<Transition> previousViterbiValues;

    //Singleton - Viterbi Algorithm never changes
    private static final Viterbi_Bigram viterbi_BP = new Viterbi_Bigram();
    public Viterbi_Bigram() { }
    public static Viterbi_Bigram getInstance(){ return viterbi_BP; }

    /**
     * SETUP Viterbi Parameters
     * @param model
     */
    public void setupViterbi(Model model) {

        this.listOfSentences = model.getListOfSentences();
        this.listOfNodes = model.getListOfNodes();
        this.listOfTags = model.getListOfTags();
        this.listOfWords = model.getListOfWords();

        this.tbl_GTValues = model.getTbl_GTValues();
        this.tbl_IPDValues = model.getTbl_IPDValues();
        this.stemmer = model.getStemmer();
        normalizer = new Normalizer(listOfTags);

        tagEmissionFound = false;
        GoodTuringSmoothing = model.isGoodTuringSmoothing();
        lst_UnknownWords = new ArrayList<>();

        totalTags_TestingSet = 0;sequenceTags = 0;perplexity = 0;
        totalWords_TestingSet = 0; num_IncorrectlyTaggedKnownWords=0;
        num_UnknownWords = 0;num_IncorrectlyTaggedUnknownWords = 0;
    }


    /**
     * --------------------------------------
     * SENTENCE FORMATTING
     * --------------------------------------
     */

    /**
     * Get tag sequence
     */
    public void getTagSequence()throws Exception
    {
        EOF = false;
        int sentNum = 0;
        String correctTag;
        previousViterbiValues = new ArrayList<>();

        //Current sentence
        correctSequence = new ArrayList<>();
        textSentence = new ArrayList<>();
        transitionsTable = new ArrayList<>();

        String words[];
        String word;

        for (String line : listOfSentences) {

            if (!line.startsWith("#") && !line.isEmpty()) {
                EOF = false;
                words = line.split("\t");
                newViterbiValues = new ArrayList<>();


                /** WORD vs LEMMA */
                word = words[1];//Word
                //word = words[2];//Lemma

                correctTag = words[3];//Universal tag
                //correctTag = words[4];//Language specific tag

                boolean tagFound = false;
                for (Tag tag : listOfTags) {
                    if (tag.getValue().equals(correctTag)) {
                        tagFound = true;
                        break;
                    }
                }
                if (!tagFound)
                    continue;

                /** Set to X or NUM Tag if applicable*/
                if(!word.matches("[^£$€%^_=+@#|~&*<>]*")) {
                    correctTag = "SYM";
                }

                for (char c : word.toCharArray()) {
                    if (Character.isDigit(c)) {
                        correctTag = "NUM"; //CD
                        break;
                    }
                }

                correctSequence.add(correctTag);
                textSentence.add(word);
                tagEmissionFound = determineEmissionValueFound(word);

                /** Get <START> transition probabilities*/
                if (words[0].equals("1")) {
                    starterTag = true;
                    stopperTag = false;
                    setViterbiValues(listOfNodes.get(0), word);

                    transitionsTable.add(newViterbiValues); //Add Viterbi values to BackPointer table
                    previousViterbiValues = newViterbiValues; //Keep pointers to previous Viterbi Values
                    starterTag = false;
                }

                /** Get all node transitions for normal sentence*/
                else {
                    for (int i = 0; i < previousViterbiValues.size(); i++) {
                        if (transitionsTable.size() == 1) //Need to switch nodes depending on starter tag
                            setViterbiValues(previousViterbiValues.get(i).getSecondNode(), word);
                        else
                            setViterbiValues(previousViterbiValues.get(i).getFirstNode(), word);
                    }

                /*
                if(newViterbiValues.size()>listOfTags.size())//3)
                    beamSearch(listOfTags.size());//3);
                    */

                    findMinTransitionPerTag();
                    transitionsTable.add(newViterbiValues);
                    previousViterbiValues = newViterbiValues;
                }


            } else {/**Calculate <STOP> transition, sequence accuracy and Reset for next sequence */
                if (line.isEmpty() && !EOF) {
                    sentNum++;
                    previousViterbiValues = newViterbiValues;
                    newViterbiValues = new ArrayList<>();

                    //Finished Normal Sentence
                    stopperTag = true;

                    //Avoid empty ending sequences: Revert back to last known tag
                    if(previousViterbiValues.size()==0)
                        previousViterbiValues = transitionsTable.get(transitionsTable.size()-1);

                    for (int i = 0; i < previousViterbiValues.size(); i++) {
                        if (transitionsTable.size() == 1) //Need to switch nodes depending on starter tag
                            setViterbiValues(previousViterbiValues.get(i).getSecondNode(), "<STOP>");
                        else
                            setViterbiValues(previousViterbiValues.get(i).getFirstNode(), "<STOP>");
                    }

                    //if(newViterbiValues.size()>listOfTags.size())//3)
                    //beamSearch(listOfTags.size());//3);
                    findMinTransitionPerTag();
                    transitionsTable.add(newViterbiValues);
                    sequenceAccuracy(correctSequence);

                    /**
                     * RESET VARIABLES
                     */
                    correctSequence.clear();
                    textSentence.clear();
                    newViterbiValues = new ArrayList<>();
                    previousViterbiValues = new ArrayList<>();
                    transitionsTable = new ArrayList<>();
                    lst_UnknownWords = new ArrayList<>();

                    //System.out.println("\nFinished Sentence Num:" + sentNum);
                    //break;
                    EOF = true;
                }
            }
        }

    }


    /**
     * Take beamWidth number of values if Viterbi gets too big
     * @param beamWidth
     */
    private void beamSearch(int beamWidth){
        List<Transition> filteredViterbiValues = new ArrayList<>();
        Transition tran;

        int i=0;
        while(i<beamWidth){
            tran = Collections.min(newViterbiValues, new CompareTransition());
            filteredViterbiValues.add(tran);
            newViterbiValues.remove(tran);
            i++;
        }

        newViterbiValues = filteredViterbiValues;
    }


    /**
     * Viterbi reduction of transitions to max probability per Tag
     */
    private void findMinTransitionPerTag(){
        List<Transition> filteredViterbiValues = new ArrayList<>();
        List<String> lst_CurrentTags = new ArrayList<>();

        for(Transition tr: newViterbiValues){
            if(!lst_CurrentTags.contains(tr.getFirstNode().getTagValue()))
                    lst_CurrentTags.add(tr.getFirstNode().getTagValue());
        }

        Transition minTran;

        for(String tag: lst_CurrentTags){
            minTran = new Transition(null,null,Double.MAX_VALUE);
            for(Transition currentTran: newViterbiValues){
                if(currentTran.getFirstNode().getTagValue().equals(tag)) {
                    if(currentTran.getTransition_Probability() < minTran.getTransition_Probability())
                        minTran = currentTran;
                }
            }
            filteredViterbiValues.add(minTran);
        }
        newViterbiValues = filteredViterbiValues;
    }


    /**
     * --------------------------------------
     * VITERBI ALGORITHM
     * --------------------------------------
     */

    /**
     * Viterbi - get max value for every node transition
     * @param givenNode
     * @param word
     * @return
     */
    public void setViterbiValues(Node givenNode, String word)
    {
        Transition logMaxTransitionValue = null;
        boolean viterbiFound;
        String givenNodeTagValue = givenNode.getTagValue();
        String transitionTagValue;

        /**
         * Zero values needed for when no value is found (0 or NaN)
         */
        boolean specialBP = false;
        double currentEmissionMax = 0;
        double currentTransitionMax = 0;
        double currentViterbiMax = 0;
        double logMax = Double.MAX_VALUE, logCurrentMax = 0, logTransVal = 0, logEmissionsVal=0, logViterbi=0;

        for(Transition tr: givenNode.getBigramTransitions()) {
            transitionTagValue = tr.getFirstNode().getTagValue();

            if (!transitionTagValue.equals("<START>") ) {
                if(stopperTag && !transitionTagValue.equals("<STOP>"))//Only want <STOP> tags at last transition
                    continue;

                /**
                 * EMISSION VALUE
                 */
                if(!stopperTag) {
                    currentEmissionMax = getEmissionValue(tr.getFirstNode(), tr.getFirstNode().getTag(), word);
                    if(currentEmissionMax == 0)
                        continue;

                    logEmissionsVal = (-1 * (Math.log(currentEmissionMax)));///Math.log(2)));
                }

                /**
                 * TRANSITION VALUE
                 */
                currentTransitionMax = tr.getTransition_Probability();
                if(currentTransitionMax == 0 || Double.isNaN(currentTransitionMax))
                    currentTransitionMax = 0.0000001;
                logTransVal = (-1*(Math.log(currentTransitionMax)));///Math.log(2)));

                /**
                 * VITERBI VALUE
                 */
                viterbiFound = false;
                //Skip Viterbi for First Starter Tag
                if(starterTag)
                    viterbiFound = true;

                //Exit once previous associative Viterbi value found
                while (!viterbiFound) {
                    for (Transition vtrbs : previousViterbiValues) {
                        //Take previous viterbi value for current tag
                        if (!starterTag && givenNodeTagValue.equals(vtrbs.getFirstNode().getTagValue()) ||
                                (transitionsTable.size()==1 && givenNodeTagValue.equals(vtrbs.getSecondNode().getTagValue()))) {
                            currentViterbiMax = vtrbs.getTransition_Probability();
                            if (currentViterbiMax == 0)
                                currentViterbiMax = 0.0000001;

                            logViterbi = currentViterbiMax;///Math.log(2)));
                            viterbiFound = true;
                            break;
                        }
                    }
                    break; //No Viterbi Value found
                }

                /**
                 * FINAL CALCULATION
                 */
                logCurrentMax = Math.exp(logEmissionsVal+logTransVal) + logViterbi;
                //STARTER or STOPPER Tag check
                specialBP = addSpecialBackpointers(logCurrentMax, givenNode, tr.getFirstNode());
                if(!specialBP){
                    //Smallest Log value equals largest probability (What minimal value for all Given-Tag Transitions)
                    logMaxTransitionValue = new Transition(tr.getFirstNode(), givenNode, (double) logCurrentMax);

                    boolean duplicateFound = false;
                    for (Transition newTrans : newViterbiValues) {
                        if (logMaxTransitionValue.getFirstNode().equals(newTrans.getFirstNode()) &&
                                (logMaxTransitionValue.getSecondNode().equals(newTrans.getSecondNode())) &&
                                (logMaxTransitionValue.getTransition_Probability() == newTrans.getTransition_Probability())) {
                            duplicateFound = true;
                            break;
                        }
                    }
                    if (!duplicateFound)
                        newViterbiValues.add(logMaxTransitionValue);
                }
            }
        }//*/
    }




    /**
     * Add Backpointer directly for Starter and Stopper tags
     * GivenNode and Transition node reversed
     * @param logCurrentMax
     * @param givenNode
     * @param transNode
     * @return
     */
    private boolean addSpecialBackpointers(double logCurrentMax, Node givenNode, Node transNode){
        Transition logMaxTransitionValue;
        //STARTER TAG VALUES
        if(starterTag || (stopperTag && transNode.getTagValue().equals("<STOP>"))) {
            logMaxTransitionValue = new Transition(givenNode, transNode, logCurrentMax);
            newViterbiValues.add(logMaxTransitionValue);
            return true;
        }
        return false;
    }



    /**
     * --------------------------------------
     * EMISSIONS
     * --------------------------------------
     * p(wi|ti) = p(unknown|ti) p(capital|ti) p(suffix|ti) p(prefix|ti)
     */

    private double getEmissionValue(Node currentNode, Tag transitionTag, String word)
    {
        double currentEmissionMax = 1;
        double captialProbability = 0, suffixProbability = 0, prefixProbability = 0;
        double wordLengthProbability = 0, goodTuringProbability = 0;
        String transitionTagValue = transitionTag.getValue();

        if(GoodTuringSmoothing)
            lst_SmoothedValues = tbl_GTValues.get(transitionTagValue); //Tag Probability Tables
        else
            lst_SmoothedValues = tbl_IPDValues.get(transitionTagValue); //Tag Probability Tables

        if (!tagEmissionFound && !transitionTagValue.equals("<STOP>")) {
            //Morphology Section
            /**
             * No Tag Value found for Word
             * Determine
             * ---------
             * If Word is
             *  NUM
             *  PROPN
             * Else
             *  Use Stemmer to determine probable tag
             */

            /** NUM Tag */
            if (morphology.isDigit()){
                if(transitionTagValue.equals("NUM") || transitionTagValue.equals("CD"))
                        return 0.9;
                    return 0; //Not a NUM if doesn't contain digit
            }

            /** X Symbol */
            if (morphology.isSymbol()){
                if(transitionTagValue.equals("SYM"))
                        return 0.9;
                    return 0; //Not an X if doesn't contain symbols
            }

            /** Length of Word to Tag probability *
            if(total_WordLength > 0) {
                int length = transitionTag.getWordLength(word.length());
                wordLengthProbability = (double) length / total_WordLength;
                currentEmissionMax = wordLengthProbability;
            }*/


            /** PROPN Tag */
            if (morphology.isCapital()){
                if(!transitionTagValue.equals("NUM")) {
                    captialProbability = transitionTag.getCapitalProbability();
                    currentEmissionMax *= captialProbability;
                }
            }

            /** Check list of Affixes for solution*/
            if (!morphology.getSuffix().equals("")) {
                if (suffixFound) {
                    suffixProbability = normalizer.getNormalized_AffixValue(transitionTag);

                    /** Account for Capital probability (PROPN can have no suffix found but still be a capital! */
                    if(morphology.isCapital() && captialProbability>0) {
                        //if (wordLengthProbability > 0) {
                            if (suffixProbability > 0)
                                currentEmissionMax *= suffixProbability;
                            else
                                currentEmissionMax *= 0.01; //Reduce probability by 90%
                       // }
                    }
                    else
                        currentEmissionMax *= suffixProbability;
                }

                /** Prefix check (Will not be a prefix if no suffix) *
                if(suffixFound && currentEmissionMax>0) {
                    if (prefixFound) {
                        prefixProbability = normalizer.getNormalized_AffixValue(transitionTag, true);

                        if(prefixProbability > 0)
                            currentEmissionMax *= prefixProbability;
                        else
                            currentEmissionMax *= 0.01; //Reduce probability by 99%
                    }
                }*/
            }

            /** Finally take smoothed GT Values for unseen objects (Check if no affix was found) */
            if (lst_SmoothedValues != null){
                if (lst_SmoothedValues.size() > 5)  //No smoothed values < 5 Count Types
                    currentEmissionMax *= lst_SmoothedValues.get(0).getProbability();
                else
                    currentEmissionMax *= 0.01;
            }
        }
        /**
         * Word found in Training Data
         * ---------------------------
         * Normal Emission Value
         * Take probability with Word frequency
         */
        else {

            wordEmissionFrequency = getWordFrequency(word, transitionTagValue);
            if (wordEmissionFrequency != 0) {
                for (int i = 0; i < lst_SmoothedValues.size(); i++) {
                    if (wordEmissionFrequency == lst_SmoothedValues.get(i).getFrequency())
                        return lst_SmoothedValues.get(i).getProbability();
                }
            }
            return 0;//No emission for this tag
        }

        /**Remove all NUM values */
        if(transitionTagValue.equals("NUM") && !morphology.isDigit()
                || transitionTagValue.equals("SYM") && !morphology.isSymbol())
            currentEmissionMax = 0;

        /** Low likelihood of PROPN Tag if not capitalised *
        if(transitionTagValue.equals("PROPN") && !morphology.isCapital() && currentEmissionMax>0)
            currentEmissionMax = 0.000001;*/

        return currentEmissionMax;
    }


    /**
     * Determine if emissions value exists
     * @param word
     * @return
     */
    private boolean determineEmissionValueFound(String word)
    {
        totalWords_TestingSet++;
        for (Tag tag: listOfTags) {
            if(!tag.getValue().equals("<START>") && !tag.getValue().equals("<STOP>")) {
                if (getWordFrequency(word, tag.getValue()) != 0) {
                    return true;
                }
            }
        }
        /**
         * Setup Morphology for Unknown Words
         */
        lst_UnknownWords.add(word);
        num_UnknownWords++;
        morphology = new Word_Morphology(word);//, stemmer.getStem(word));
        suffixFound = normalizer.existsAffix(morphology.getSuffix());//Check if Affix existed from Training
        return false;
    }


    /**
     * Get Emission Probability
     * @param textWord
     * @return
     */
    public int getWordFrequency(String textWord, String tag){

        for(Word word: listOfWords) {
            if (textWord.equals(word.getValue()) && (tag.equals(word.getTag().getValue()))) {
                return word.getFrequency();
            }
        }

        return 0;
    }

    /**
     * --------------------------------------
     * ACCURACY CALCULATIONS
     * --------------------------------------
     */

    /**
     * Get overall accuracy
     */
    public void calculateAccuracy() {

        //System.out.println("Total Words:" + totalTags_TestingSet);

        System.out.println("Unknown Words: " + (double)num_UnknownWords/totalWords_TestingSet);

        System.out.println("Unknown Words Accuracy: " + (double)(num_UnknownWords - num_IncorrectlyTaggedUnknownWords)
                /num_UnknownWords);

        System.out.println("Known Words Accuracy:" + ((double)totalTags_TestingSet - num_UnknownWords - num_IncorrectlyTaggedKnownWords)
                / (totalTags_TestingSet - num_UnknownWords));

        System.out.println("Sequence Accuracy: " + ((double)sequenceTags/totalTags_TestingSet) +"\n");
    }



    /**
     * Retrieve most Probable Sequence from BackPointer table
     * Compare Probable Sequence vs Actual Sequence
     *
     * @param correctSequence
     */
    private void sequenceAccuracy(List<String> correctSequence){
        //System.out.println(textSentence.get(0));

        String sequenceArray[] = new String[correctSequence.size()];

        /*** Retrieve most probable sequence from BackPointers*/
        for(int i=1; i<transitionsTable.size();i++) {
            Transition tran = Collections.min(transitionsTable.get(i), Comparator.comparing(tr -> tr.getTransition_Probability()));
            sequenceArray[i-1] = tran.getSecondNode().getTagValue();

            if (i == transitionsTable.size()-1)
                sequenceArray[i-1] = tran.getFirstNode().getTagValue();
        }

        boolean knownWord;
        /*** Compare Probable Sequence vs Actual Sequence*/
        for(int j=0;j<sequenceArray.length;j++) {
            knownWord = true;
            totalTags_TestingSet++;

            if (sequenceArray[j].equals(correctSequence.get(j))){
                sequenceTags++;
            }
            else {
                //System.out.println(textSentence.get(j)+"->"+correctSequence.get(j) + ":" + sequenceArray[j]);
                for(String unk: lst_UnknownWords){
                    if(unk.equals(textSentence.get(j))) {
                        //System.out.println("UNK " + textSentence.get(j)+"->"+correctSequence.get(j) + ":" + sequenceArray[j]+"\n");
                        num_IncorrectlyTaggedUnknownWords++;
                        knownWord = false;
                        break;
                    }
                }

                if(knownWord)
                    num_IncorrectlyTaggedKnownWords++;
            }
        }
    }


}


/**
 * Compare transitions
 */
class CompareTransition implements Comparator<Transition> {
    @Override
    public int compare(Transition t1, Transition t2) {
        return Double.compare(t1.getTransition_Probability(),t2.getTransition_Probability());
    }
}
