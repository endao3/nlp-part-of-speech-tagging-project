package Probabilities;

import HMM.Node;

import java.util.Comparator;

public class Transition {

    private double discount = 0.75;
    private double interpolation = 1;

    private Node givenNode, firstNode, secondNode;
    private int numTransitions;
    private double transition_Probability;

    //Bigram Transition
    public Transition(Node firstNode) {
        this.firstNode = firstNode;
        incrementTransitions();
        transition_Probability = 0;
    }

    //Trigram Transition
    public Transition(Node firstNode, Node secondNode) {
        this.firstNode = firstNode;
        this.secondNode = secondNode;
        incrementTransitions();
        transition_Probability = 0;
    }

    //Viterbi Bigram Transition
    public Transition(Node firstNode,Node givenNode, double transition_Probability) {
        this.firstNode = firstNode;
        this.secondNode = givenNode;
        this.transition_Probability = transition_Probability;
    }

    //Viterbi Trigram Transition
    public Transition(Node firstNode,Node secondNode, Node givenNode, double transition_Probability) {
        this.givenNode = givenNode;
        this.firstNode = firstNode;
        this.secondNode = secondNode;
        this.transition_Probability = transition_Probability;
    }

    public Node getFirstNode() {
        return firstNode;
    }

    public Node getSecondNode() {
        return secondNode;
    }

    public Node getGivenNode() { return givenNode; }

    public void incrementTransitions(){ numTransitions++;}

    public void decrementTransitions(){ numTransitions--; }

    public int getNumTransitions(){ return numTransitions; }

    public double getTransition_Probability() { return transition_Probability; }

    public void setTransition_Probability(double transition_Probability) {
        this.transition_Probability = transition_Probability;
    }
}