package Smoothing;

import HMM.Node;
import HMM.Tag;
import HMM.Word;
import Probabilities.Transition;

import java.util.*;

public class InterpolatedDiscounting {

    private int missingNodes;
    private double transitionDiscount = 0.75;
    private double emissionDiscount = 0.75;
    private double interpolation = 1;

    public InterpolatedDiscounting(){ }


    /**
     * SMOOTHING EMISSIONS
     * -------------------
     * Get Count:Frequency for each Tag using word counts
     *
     * 1. Iterate through Tags
     * 2. Determine counts/frequencies for all words per Tag
     * 3. Discounting: MLE Modified
     * 4. Interpolation: Normalizing Constant used to redistribute the probability mass we've discounted
     *
     * @param listOfWords
     * @param listOfTags
     */
    public Map<String,List<SmoothedProbability>> setEmission_IPDTables(List<Tag> listOfTags, List<Word> listOfWords){
        Map<String,List<SmoothedProbability>> tbl_IPDValues = new HashMap<>();
        List<SmoothedProbability> lst_SmoothedValues;

        int tagFrequency, wordTagCount;
        double discountedMLE_Value;

        /** Map to hold all counts for IPD*/
        int key, value;
        Map<Integer, Integer> map_IPDCounts;

        /** 1. Iterate through Tags
         * -----------------------
         * */
        for(Tag tag: listOfTags) {
            lst_SmoothedValues = new ArrayList<>();
            map_IPDCounts = new TreeMap<>();
            wordTagCount = 0;

            /** 2. Determine counts
             * -------------------
             * Check if word frequency exists in Map
             * IF missing: Add new count value to map
             * ELSE: Increment freq for count value
             */
            if (!tag.equals("<START>") && !tag.equals("<STOP>")) {
                for (Word word : listOfWords) {
                    if (word.getTag().equals(tag)) {
                        wordTagCount++;
                        key = word.getFrequency();
                        tagFrequency = tag.getFrequency();

                        //Set counts for frequencies per word/tag
                        if (!map_IPDCounts.containsKey(key)) {
                            map_IPDCounts.put(key, 1);
                            discountedMLE_Value = ((double)key-emissionDiscount) / tagFrequency;
                            lst_SmoothedValues.add(new SmoothedProbability(key, discountedMLE_Value));
                        }
                        else {
                            value = map_IPDCounts.get(key);
                            value++;
                            map_IPDCounts.put(key, value);
                        }
                    }
                }
            }


            /**
             * 3. Interpolated Discounting: MLE Modified
             * -------------------
             * Smooth Values using IPD
             * discount /  Count(current tag total frequency) * (list of affected words)
             */
            interpolation = ((double)(emissionDiscount/tag.getFrequency()) * (lst_SmoothedValues.size()));

            //Unigram Probability
            interpolation *= tag.getOverallProbability();

            for(SmoothedProbability emissionValue: lst_SmoothedValues){
                double discountedValue = emissionValue.getProbability();
                discountedValue += interpolation;
                emissionValue.setProbability(discountedValue);
            }

            double probability=0;
            /** 4. Add list of new Smoothed values to table*/
            if(lst_SmoothedValues.size()>0) {
                /** Add in Unknown Word variable */
                double unknownWordProbability = emissionDiscount / wordTagCount;//(listOfTags.size()-this.missingNodes-2);
                lst_SmoothedValues.add(new SmoothedProbability(0,unknownWordProbability));
                probability+=unknownWordProbability;
                for(SmoothedProbability sp: lst_SmoothedValues)
                    probability += sp.getProbability();

                //System.out.println(probability);
                Collections.sort(lst_SmoothedValues, new SortSmoothedProbabilityByBestValue());
                tbl_IPDValues.put(tag.getValue(), lst_SmoothedValues);
            }
        }

        return tbl_IPDValues;
    }




    /**
     * SMOOTHING TRANSITIONS
     * ---------------------
     * Take 'discount' off every transition for current Tag Frequency > 0
     * Discount is redistributed to Zero occurring N-Grams
     *
     * 1. Discounting: MLE Modified
     * 2. Interpolation: Normalizing constant used to redistribute the probability mass we've discounted
     * --
     * 3. MLE + Normalizing Constant
     *
     * Set new probability to Transition
     *
     * @param transition
     * @param totalPreviousTags
     * @param missingNodes
     * @param typeTrigram
     */
    public void setTransition_Probability(Transition transition, int totalPreviousTags, int missingNodes, boolean typeTrigram){

        transition.decrementTransitions(); //Added in extra connection to create Fully Connected FSA. (Remove for Probability calculation)

        this.missingNodes = missingNodes;
        double transition_Probability = 0;
        int numTransitions = transition.getNumTransitions();

        Node firstNode = transition.getFirstNode();
        Node secondNode = transition.getSecondNode();

        //If Trigram model: Search list of Bigrams for previous frequency
        if(typeTrigram)
        {
            for(Transition tr: firstNode.getBigramTransitions())
            {
                if(tr.getFirstNode().compareTo(secondNode)) {
                    totalPreviousTags = tr.getNumTransitions();
                    break;
                }
            }
        }

        /** 1. Discounting: MLE Modified */
        if(numTransitions>0) {
            transition_Probability = (numTransitions - transitionDiscount) / (totalPreviousTags);
        }

        /**
         * 2. Interpolation: Zero values is number of nodes not in FSA (couple of tags not found in training set)
         *
         * (19-zeroValues or numTransitions;
         * The number of word types that we discounted;
         * In other words, the number of times we applied the normalized discount.)
         */
        // Context
        if(totalPreviousTags > 0)
            interpolation = ((double)(transitionDiscount/totalPreviousTags) * (19-this.missingNodes));

        // Unigram
        if(!typeTrigram)
            interpolation *= firstNode.getTag().getOverallProbability();
        else
            interpolation *= secondNode.getTag().getOverallProbability();

        /** 3. Interpolated Discounting */
        transition_Probability += interpolation;

        transition.setTransition_Probability(transition_Probability);
    }
}

/**
 * Sort by alphabet
 */
class SortSmoothedProbabilityByBestValue implements Comparator<SmoothedProbability>
{
    @Override
    public int compare(SmoothedProbability n1, SmoothedProbability n2)
    {
        if(n1.getFrequency() < n2.getFrequency())
            return -1;
        else if(n1.getFrequency() > n2.getFrequency())
            return 1;
        return 0;
    }
}
