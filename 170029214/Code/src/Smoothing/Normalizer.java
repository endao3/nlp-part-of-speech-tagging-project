package Smoothing;

import HMM.Tag;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Normalize probability values per tag/transition
 * 0 < n < 1
 */
public class Normalizer {

    private List<Tag> listOfTags;
    private int suffixSum = 0, prefixSum = 0;
    private String suffix = "", prefix = "";
    //TODO Transition Smoothing - rescale


    public Normalizer(List<Tag> lstOfTags) {
        this.listOfTags = lstOfTags;
    }

    /**
     * Normalize transitions
     * Sum all probabilities
     * Get individual probability for each tag in transition array
     * @param transitionValues
     * @param totalProbability
     * @return
     */
    public static double[] getNormalized_TransitionValues(double[] transitionValues, double totalProbability) {
        double[] result = new double[transitionValues.length];
        for(int i=0;i<transitionValues.length;i++){
            result[i] = transitionValues[i]/totalProbability;
        }
        return result;
    }


    /**
     * Rescaling to a sum of 1
     * @return
     */
    public double getNormalized_AffixValue(Tag currentTag) {
        int sum, value = 0;
        String currentAffix = this.suffix;

        if(!currentTag.equals("NUM") || !currentTag.equals("SYM"))
            value += currentTag.getAffixFrequency(currentAffix);

        sum = suffixSum;

        return ((double)value/sum);
    }

    /**
     * Check if any possible Affix exists
     * Return True if sum > 0
     *        False if sum == 0
     * @param affix
     * @return
     */
    public boolean existsAffix(String affix){
        if(affix.equals(""))
            return false;

        int sum = 0;
        String currentAffix;

        /**
         * Sum all values with Affix in Tag Mapping
         */

        this.suffix = affix.toLowerCase();
        currentAffix = this.suffix;

        for (Tag tag : listOfTags) {
            if(!tag.getValue().equals("NUM") || !tag.equals("SYM"))
                sum += tag.getAffixFrequency(currentAffix);
        }


        if(sum == 0)
            return false;

        suffixSum = sum;
        return true;
    }

 }