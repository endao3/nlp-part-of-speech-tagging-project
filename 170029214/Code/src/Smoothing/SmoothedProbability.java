package Smoothing;

/**
 * Use as replacement for Emissions relative frequency probabilities during smoothing
 */
public class SmoothedProbability {

    private int frequency;
    private double probability;

    /**Emissions Constructor */
    public SmoothedProbability(int frequency, double probability) {
        this.frequency = frequency;
        this.probability = probability;
    }

    public int getFrequency() {
        return frequency;
    }

    public double getProbability() {
        return probability;
    }

    public void setProbability(double probability) {
        this.probability = probability;
    }

}