package Smoothing;
/*
TODO
Laplace smoothing:
    Add 1
    Pretend everything occurs at least once

Add-k smoothing
    Add k>0

Good-Turing smoothing
    ***
    *
    *


//package edu.stanford.nlp.stats;
/**
 * Stanford Library Simple Good-Turing Smoothing
 */

import HMM.Node;
import HMM.Tag;
import HMM.Word;
import Probabilities.Transition;

import java.util.*;

/**
 * Simple Good-Turing smoothing, based on code from Sampson, available at:
 * ftp://ftp.informatics.susx.ac.uk/pub/users/grs2/SGT.c <p/>
 *
 * See also http://www.grsampson.net/RGoodTur.html
 *
 * @author Bill MacCartney (wcmac@cs.stanford.edu)
 */
public class SimpleGoodTuring {

    private static final int MIN_INPUT = 5;
    private static final double CONFID_FACTOR = 1.96;
    private static final double TOLERANCE = 1e-12;

    private int[] r;               // for each bucket, a frequency
    private int[] n;               // for each bucket, number of items w that frequency
    private int rows;              // number of frequency buckets

    private int bigN = 0;          // total count of all items
    private double pZero;          // probability of unseen items
    private double bigNPrime;
    private double slope;
    private double intercept;
    private double[] z;
    private double[] logR;
    private double[] logZ;
    private double[] rStar;
    private double[] p;

    /**
     * Each instance of this class encapsulates the computation of the smoothing
     * for one probability distribution.  The constructor takes two arguments
     * which are two parallel arrays.  The first is an array of counts, which must
     * be positive and in ascending order.  The second is an array of
     * corresponding counts of counts; that is, for each i, n[i] represents the
     * number of types which occurred with count r[i] in the underlying
     * collection.  See the documentation for main() for a concrete example.
     */
    public SimpleGoodTuring() { }

    private void computeSGT(int[] r, int[] n) {
        if (r == null) throw new IllegalArgumentException("r must not be null!");
        if (n == null) throw new IllegalArgumentException("n must not be null!");
        if (r.length != n.length) throw new IllegalArgumentException("r and n must have same size!");
        if (r.length < MIN_INPUT) throw new IllegalArgumentException("r must have size >= " + MIN_INPUT + "!");

        this.r = new int[r.length];
        this.n = new int[n.length];

        System.arraycopy(r, 0, this.r, 0, r.length); // defensive copy
        System.arraycopy(n, 0, this.n, 0, n.length); // defensive copy
        this.rows = r.length;

        compute();
        validate(TOLERANCE);
    }


    /**
     * Returns the probability allocated to types not seen in the underlying
     * collection.
     */
    public double getProbabilityForUnseen() {
        return pZero;
    }

    /**
     * Returns the probabilities allocated to each type, according to their count
     * in the underlying collection.  The returned array parallels the arrays
     * passed in to the constructor.  If the returned array is designated p, then
     * for all i, p[i] represents the smoothed probability assigned to types which
     * occurred r[i] times in the underlying collection (where r is the first
     * argument to the constructor).
     */
    public double[] getProbabilities() {
        return p;
    }

    private void compute() {
        int i, j, next_n;
        double k, x, y;
        boolean indiffValsSeen = false;

        z = new double[rows];
        logR = new double[rows];
        logZ = new double[rows];
        rStar = new double[rows];
        p = new double[rows];

        for (j = 0; j < rows; ++j) bigN += r[j] * n[j]; // count all items
        next_n = row(1);
        pZero = (next_n < 0) ? 0 : n[next_n] / (double) bigN;
        for (j = 0; j < rows; ++j) {
            i = (j == 0 ? 0 : r[j - 1]);
            if (j == rows - 1)
                k = (double) (2 * r[j] - i);
            else
                k = (double) r[j + 1];
            z[j] = 2 * n[j] / (k - i);
            logR[j] = Math.log(r[j]);
            logZ[j] = Math.log(z[j]);
        }
        findBestFit();
        for (j = 0; j < rows; ++j) {
            y = (r[j] + 1) * smoothed(r[j] + 1) / smoothed(r[j]);
            if (row(r[j] + 1) < 0)
                indiffValsSeen = true;
            if (!indiffValsSeen) {
                x = (r[j] + 1) * (next_n = n[row(r[j] + 1)]) / (double) n[j];
                if (Math.abs(x - y) <= CONFID_FACTOR * Math.sqrt(sq(r[j] + 1.0)
                        * next_n / (sq((double) n[j]))
                        * (1 + next_n / (double) n[j])))
                    indiffValsSeen = true;
                else
                    rStar[j] = x;
            }
            if (indiffValsSeen)
                rStar[j] = y;
        }
        bigNPrime = 0.0;
        for (j = 0; j < rows; ++j)
            bigNPrime += n[j] * rStar[j];
        for (j = 0; j < rows; ++j)
            p[j] = (1 - pZero) * rStar[j] / bigNPrime;
    }

    /**
     * Returns the index of the bucket having the given frequency, or else -1 if no
     * bucket has the given frequency.
     */
    private int row(int freq) {
        int i = 0;
        while (i < rows && r[i] < freq) i++;
        return ((i < rows && r[i] == freq) ? i : -1);
    }

    private void findBestFit() {
        double XYs, Xsquares, meanX, meanY;
        int i;
        XYs = Xsquares = meanX = meanY = 0.0;
        for (i = 0; i < rows; ++i) {
            meanX += logR[i];
            meanY += logZ[i];
        }
        meanX /= rows;
        meanY /= rows;
        for (i = 0; i < rows; ++i) {
            XYs += (logR[i] - meanX) * (logZ[i] - meanY);
            Xsquares += sq(logR[i] - meanX);
        }
        slope = XYs / Xsquares;
        intercept = meanY - slope * meanX;
    }

    private double smoothed(int i) {
        return (Math.exp(intercept + slope * Math.log(i)));
    }

    private static double sq(double x) {
        return (x * x);
    }

    public void print() {
        int i;
        System.out.println("Number of items" + bigN);
        System.out.printf("%6s %6s %8s %8s%n", "r", "n", "p", "p*");
        System.out.printf("%6s %6s %8s %8s%n", "----", "----", "----", "----");
        System.out.printf("%6d %6d %8.4g %8.4g%n", 0, 0, 0.0, pZero);
        for (i = 0; i < rows; ++i)
            System.out.printf("%6d %6d %8.4g %8.4g%n", r[i], n[i], 1.0 * r[i] / bigN, p[i]);
    }

    /**
     * Ensures that we have a proper probability distribution.
     */
    private void validate(double tolerance) {
        double sum = pZero;
        for (int i = 0; i < n.length; i++) {
            sum += (n[i] * p[i]);
        }
        double err = 1.0 - sum;
        if (Math.abs(err) > tolerance) {
            throw new IllegalStateException("ERROR: the probability distribution sums to " + sum);
        }
    }

    /**
     * EMISSION GT TABLES
     * ---------------
     * Get Count:Frequency for each Tag using word counts
     *
     * 1. Iterate through Tags
     * 2. Determine counts/frequencies for all words per Tag
     * 3. Get Good Turing Estimates if more than five different Count types occurrences
     * 4. Add smoothed(or original) values to table with Tag Pointer per column
     */
    public Map<String,List<SmoothedProbability>> setEmission_GTTables(List<Tag>listOfTags, List<Word>listOfWords)
    {
        Map<String,List<SmoothedProbability>> tbl_GTValues = new HashMap<>();
        List<SmoothedProbability> lst_SmoothedValues;
        List<Double> lst_OriginalValues;

        /** Map to hold all counts to be passed to GT*/
        int key, value;
        Map<Integer, Integer> map_GTCounts;

        /** 1. Iterate through Tags
         * -----------------------
         * */
        for(Tag tag: listOfTags) {
            lst_SmoothedValues = new ArrayList<>();
            lst_OriginalValues = new ArrayList<>();
            map_GTCounts = new TreeMap<>();

            /** 2. Determine counts
             * -------------------
             * Check if word frequency exists in Map
             * IF missing: Add new count value to map
             * ELSE: Increment freq for count value
             */
            if (!tag.equals("<START>") && !tag.equals("<STOP>")) {
                for (Word word : listOfWords) {
                    if (word.getTag().equals(tag)) {
                        key = word.getFrequency();
                        if (!map_GTCounts.containsKey(key)) {
                            map_GTCounts.put(key, 1);
                            lst_OriginalValues.add(word.getEmission());
                        } else {
                            value = map_GTCounts.get(key);
                            value++;
                            map_GTCounts.put(key, value);
                        }
                    }
                }
            }

            /**
             * 3. GT Estimates > 5
             * -------------------
             * Smooth Values using Good Turing
             */
            if (!map_GTCounts.isEmpty()) {

                //Must convert to int arrays for GT Smoothing algorithm
                int[] keys = new int[map_GTCounts.size()];
                int[] values = new int[map_GTCounts.size()];
                int i = 0;

                for (Map.Entry<Integer, Integer> entry : map_GTCounts.entrySet()) {
                    keys[i] = entry.getKey();
                    values[i] = entry.getValue();
                    i++;
                }

                //If Count types > 5 for Tag
                if (keys.length > 5) {
                    this.computeSGT(keys, values);

                    /** Add unseen probability value to list */
                    double[] probs = getProbabilities();
                    lst_SmoothedValues.add(new SmoothedProbability(0, getProbabilityForUnseen()));

                    /** Add all other values with frequencies to list */
                    for (int j = 0; j < probs.length; j++) {
                        lst_SmoothedValues.add(new SmoothedProbability(keys[j], probs[j]));
                    }
                }
                //Else: Use original un-smoothed values
                else {
                    Collections.sort(lst_OriginalValues);
                    for (int j = 0; j < values.length; j++) {
                        lst_SmoothedValues.add(new SmoothedProbability(keys[j], lst_OriginalValues.get(j)));
                    }
                }

                /** 4. Add list of new Smoothed values to table
                 * ------------------------------------
                 */
                tbl_GTValues.put(tag.getValue(), lst_SmoothedValues);
            }
        }
        return tbl_GTValues;
    }



    /**
     * TRANSITION GT TABLES
     * -----------------
     */
    public Map<String,List<SmoothedProbability>> setTransition_GTTables(Node currentNode, List<Transition> lst_NGramTransitions)
    {
        Map<String,List<SmoothedProbability>> tbl_GTValues = new HashMap<>();
        List<SmoothedProbability> lst_SmoothedValues = new ArrayList<>();

        /** Map to hold all counts to be passed to GT*/
        int key, value;
        Map<Integer, Integer> map_GTCounts = new TreeMap<>();

        /** 1. Iterate through Tags
         * -----------------------
         * */
        for(Transition tr: lst_NGramTransitions) {
            tr.decrementTransitions();

            /** 2. Determine counts
             * -------------------
             * Check if word frequency exists in Map
             * IF missing: Add new count value to map
             * ELSE: Increment freq for count value
             */
            key = tr.getNumTransitions();
            if (key > 0) {
                if (!map_GTCounts.containsKey(key)) {
                    map_GTCounts.put(key, 1);
                } else {
                    value = map_GTCounts.get(key);
                    value++;
                    map_GTCounts.put(key, value);
                }
            }
        }

        /**
         * 3. GT Estimates
         * -------------------
         * Smooth Values using Good Turing
         */

        //Must convert to int arrays for GT Smoothing algorithm
        int[] keys = new int[map_GTCounts.size()];
        int[] values = new int[map_GTCounts.size()];
        int i = 0;

        for (Map.Entry<Integer, Integer> entry : map_GTCounts.entrySet()) {
            keys[i] = entry.getKey();
            values[i] = entry.getValue();
            i++;
        }

        /** SGT Algorithm */
        this.computeSGT(keys, values);

        /** Add unseen probability value to list */
        double[] probs = getProbabilities();
        lst_SmoothedValues.add(new SmoothedProbability(0, getProbabilityForUnseen()));

        /** Add all other values with frequencies to list */
        for (int j = 0; j < probs.length; j++) {
            lst_SmoothedValues.add(new SmoothedProbability(keys[j], probs[j]));
        }

        /** 4. Add list of new Smoothed values to table
         * ------------------------------------
         */
        tbl_GTValues.put(currentNode.getTagValue(), lst_SmoothedValues);

        return tbl_GTValues;
    }


    /**
     * Good Turing basic formula
     * -------------------------
     * C(t) = 0: N1/n
     *
     * C(t) = C(t)+1 * N(C(t)+1 / n*N(C(t)
     * @param currentNode
     * @param lst_NGramTransitions
     */
    public void setTransition_Probabilities(Node currentNode, List<Transition> lst_NGramTransitions)
    {

        boolean transitionFound = false;
        List<SmoothedProbability> lst_SmoothedValues = new ArrayList<>();

        /** Map to hold all counts to be passed to GT*/
        int key;
        List<Integer> lst_TransitionProbabilities = new ArrayList<>();

        /** 1. Iterate through Tags
         * -----------------------
         * */
        for(Transition tr: lst_NGramTransitions) {
            tr.decrementTransitions();

            //<STOP> never comes before any tag
            if(currentNode.getTag().getValue().equals("<STOP>")){
                tr.setTransition_Probability(0);
            }

            else {
                /** 2. Determine counts
                 * -------------------
                 * Check if word frequency exists in Map
                 * IF missing: Add new count value to map
                 * ELSE: Increment freq for count value
                 */
                key = tr.getNumTransitions();
                if (key > 0) {
                    lst_TransitionProbabilities.add(key);
                    transitionFound = true;
                }

            }
        }

        /**
         * 3. Convert list to array
         * Get min value index
         * ------------------------
         */

        if(transitionFound) {
            //Must convert to ordered integer arrays for GT Smoothing algorithm
            Integer[] keys = new Integer[lst_TransitionProbabilities.size()];
            List<Integer> orderedProbabilities = lst_TransitionProbabilities;
            Collections.sort(orderedProbabilities);
            keys = orderedProbabilities.toArray(keys);

            /**
             * 4. GOOD TURING CALCULATION
             * -------------------------
             */

            int totalTags = currentNode.getTag().getFrequency();
            int ct_1, nt1, nt;
            double newTransitionProbability, totalSum = 0;


            /** Add unknown word (zero count) value to list
             *  Add all other values with frequencies to list
             *  Only want smoothed values for low occurrences: < 5% of totalTags
             *  c(t)+1 * n(t+1)) / n(t) * totalTags
             */

            double unseenProbability = ((double) keys[0]) / totalTags;

            for (int j = 0; j < keys.length; j++) {

                if (keys[j] < totalTags / 20 && !currentNode.getTag().getValue().equals("<STOP>")) {
                    ct_1 = keys[j] + 1; //c(t)+1
                    nt1 = keys[j + 1];//n(t+1)
                    nt = keys[j];//n(t)

                    newTransitionProbability = ((double) ct_1 * nt1) / (nt * totalTags);
                } else
                    newTransitionProbability = ((double) keys[j]) / totalTags;

                lst_SmoothedValues.add(new SmoothedProbability(keys[j], newTransitionProbability));
            }

            /** Normalize probabilities
             *  Require original transition order with new smoothed values
             *  Normalize all values to equal 1
             */
            double[] smoothedValues = new double[lst_NGramTransitions.size()];
            int i = 0;

            for (Transition tr : lst_NGramTransitions) {
                if (tr.getNumTransitions() == 0) {
                    smoothedValues[i] = unseenProbability;
                    totalSum += unseenProbability;
                    i++;
                } else {
                    for (SmoothedProbability sp : lst_SmoothedValues) {
                        if (tr.getNumTransitions() == sp.getFrequency()) {
                            smoothedValues[i] = sp.getProbability();
                            totalSum += sp.getProbability();
                            i++;
                            break;
                        }
                    }
                }
            }

            normalizeProbabilities(lst_NGramTransitions, smoothedValues, totalTags, totalSum);
        }
    }


    /**
     * Normalizing method for transition probabilities
     * @param lst_NGramTransitions
     * @param smoothedValues
     * @param totalNumberOfTags
     * @param totalSum
     */
    private void normalizeProbabilities(List<Transition> lst_NGramTransitions, double[] smoothedValues,
                                        int totalNumberOfTags, double totalSum)
    {
        /** Reset normalized probabilities to all tags */
        if(totalNumberOfTags > 0) {
            //Normalizing
            smoothedValues = Normalizer.getNormalized_TransitionValues(smoothedValues, totalSum);

            int i = 0;
            for (Transition tr : lst_NGramTransitions) {
                tr.setTransition_Probability(smoothedValues[i]);
                i++;
            }
        }
    }


    // main =======================================================================

    /**
     * Unknown words in the task model (2)To capture the fact that some POS classes are more likely to generate unknown words,
     * we can simply use Good-Turing smoothing on P(w|t).
     *
     * n0: number of unknown words.
     * n1(t): number of words which appeared once with tag-t.
     * N(t): total number of words which appeared with tag-t.
     *
     * Good-Turing estimate for unknown word u:
     * P(u|t) = n1(t) / n0 * N(t)
     *
     * Like Sampson's SGT program, reads data from STDIN and writes results to
     * STDOUT.  The input should contain two integers on each line, separated by
     * whitespace.  The first integer is a count; the second is a count for that
     * count.  The input must be sorted in ascending order, and should not contain
     * 0s.  For example, valid input is: <p/>
     *
     * <pre>
     *   1 10
     *   2 6
     *   3 4
     *   5 2
     *   8 1
     * </pre>
     *
     * This represents a collection in which 10 types occur once each, 6 types
     * occur twice each, 4 types occur 3 times each, 2 types occur 5 times each,
     * and one type occurs 10 times, for a total count of 52.  This input will
     * produce the following output: </p>
     *
     * <pre>
     *     r      n        p       p*
     *  ----   ----     ----     ----
     *     0      0    0.000   0.1923
     *     1     10  0.01923  0.01203
     *     2      6  0.03846  0.02951
     *     3      4  0.05769  0.04814
     *     5      2  0.09615  0.08647
     *     8      1   0.1538   0.1448
     * </pre>
     *
     * The last column represents the smoothed probabilities, and the first item
     * in this column represents the probability assigned to unseen items.
     *
    public static void main(String[] args) throws Exception {
        int[][] input = readInput();
        SimpleGoodTuring sgt = new SimpleGoodTuring(input[0], input[1]);
        sgt.print();
    }*/


}
