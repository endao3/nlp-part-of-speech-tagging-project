package IO_Operations;
//Superclass for all common IO Operations

import java.io.File;

public abstract class IO_Controller {

    protected File file;
    protected StringBuilder builder;

    public IO_Controller(StringBuilder builder) {
        this.builder = builder;
    }
}
