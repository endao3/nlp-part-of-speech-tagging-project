package IO_Operations;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class WriteOutput extends IO_Controller{

    public WriteOutput() {
        super(new StringBuilder());
    }

    /**
     * Write probabilities to file in UTF-8 format
     * @param file
     * @param string
     * @throws IOException
     */
    public void writeOutput(String file, StringBuilder string) throws IOException
    {
        Writer writer = null;
        BufferedWriter out = null;

        FileWriter fileWriter;
        try {
            /*
            fileWriter = new FileWriter(file);
            PrintWriter printWriter = new PrintWriter(fileWriter, false);
            printWriter.print(string.toString());
            printWriter.close();*/

            writer = new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8);
            out = new BufferedWriter(writer);
            out.write(string.toString());
            out.close();

        } catch (IOException e) {
        e.printStackTrace();
        }
    }


}
