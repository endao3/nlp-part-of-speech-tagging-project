package Parsers;
/*
 First-Order Markov Assumption
 Tag depends on previous tag only

 p(Yi|Yi-1)
 */

import HMM.Node;

import java.util.List;

public class Bigram extends NGram{

    int counter = 0;

    public Bigram(List<String> lst_Sentences, List<Node> lst_Nodes) {
        super(2, lst_Sentences, lst_Nodes);
    }

    /**
     * TODO
     * P(ti|ti-1) for every state
     * Determine if state is eligible initial/final state
     */
    public List<Node> calculateTransitionProbabilities(boolean GoodTuringSmoothing)
    {
        String currentTagValue, nextTagValue;
        String currentLine,nextLine;
        String currentWord, nextWord;
        String currentWords[], nextWords[];

        //Go through text file
        for (int i = 0; i< lst_Sentences.size(); i++) {
            currentLine = lst_Sentences.get(i);

            //Look for text from sentence only - skip setup info
            if(!currentLine.startsWith("#") && !currentLine.isEmpty()) {
                currentWords = currentLine.split("\t");

                //Go through all nodes in FSA - Find node with tag: currentWords[3] is lowercase word from text
                for (Node currentNode : lst_Nodes) {

                    /** WORD VS LEMMA */
                    currentWord = currentWords[1];//Word
                    //currentWord = currentWords[2];//Lemma

                    currentTagValue = currentWords[3];//Universal tag
                    //currentTagValue = TagStrings.checkTag(currentWords[4]);//Language specific tag

                    /** Set to X or NUM Tag if applicable*/
                    if(!currentWord.matches("[^£$€%^_=+@#|~&*<>]*")) {
                        currentTagValue = "SYM";
                    }

                    for (char c : currentWord.toCharArray()) {
                        if (Character.isDigit(c)) {
                            currentTagValue = "NUM"; //CD
                            break;
                        }
                    }


                    if(currentTagValue.equals(currentNode.getTagValue()))
                    {
                        //Check if Node can be Initial Node for FSA - Add <START> tag transition
                        if (currentWords[0].equals("1")) {
                            currentNode.setInitialNode(true);
                            lst_Nodes.get(0).addTransition(currentNode, null, false);
                            counter++;
                        }

                        //Determine the following tag (First Order/Bigram)
                        nextLine = lst_Sentences.get(i + 1);

                        if (!nextLine.isEmpty()) { //Not at end of sentence
                            nextWords = nextLine.split("\t");

                            /** WORD VS LEMMA */
                            nextWord = nextWords[1];//Word
                            //nextWord = nextWords[2];//Lemma

                            nextTagValue = nextWords[3];//Universal tag
                            //nextTagValue = TagStrings.checkTag(nextWords[4]);//Language specific tag

                            /** Set to X or NUM Tag if applicable*/
                            if(!nextWord.matches("[^£$€%^_=+@#|~&*<>]*")) {
                                nextTagValue = "SYM";
                            }

                            for (char c : currentWord.toCharArray()) {
                                if (Character.isDigit(c)) {
                                    nextTagValue = "NUM"; //CD
                                    break;
                                }
                            }


                            //Add new Node transition based on following tag
                            for (Node nextNode : lst_Nodes) {
                                if (nextTagValue.equals(nextNode.getTagValue())) {
                                    currentNode.addTransition(nextNode, null, false);
                                    counter++;
                                    break;
                                }
                            }
                        } else { //Node can be Closure Node for FSA - Add <STOP> transition
                            currentNode.setClosureNode(true);
                            currentNode.addTransition(lst_Nodes.get(lst_Nodes.size() - 1), null, false);
                            counter++;
                        }
                    }
                }
            }
        }

        //Remove all empty connections to other nodes: Add one connection for every node
        for(Node node1: lst_Nodes){
            for(Node node2: lst_Nodes) {
                node1.addTransition(node2, null, false);
            }
        }

        setTransitionProbabilities(GoodTuringSmoothing);
        System.out.println("Bigrams:" + counter);
        return lst_Nodes;

    }

    /**Set transition probability using total tag frequency
     * P(NN | DT) = C(DT NN)/C(DT)
     */
    public void setTransitionProbabilities(boolean GoodTuringSmoothing)
    {
        for(Node node: lst_Nodes) {
            node.setTransitionProbability(node.getTag().getFrequency(),GoodTuringSmoothing, false);
        }
    }

    /**
     * Return Bigram Transition details
     */
    public String printTransitionProbabilities()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("Bigram Transition Probabilities\n" +
                       "*******************************");

        for(Node node: lst_Nodes) {
            builder.append(node.toString() + "\n");
            for(Node node2: lst_Nodes) {
                builder.append(node.printTransitionProbability(node2) + "\n");
            }
        }

        return builder.toString();
    }

}
