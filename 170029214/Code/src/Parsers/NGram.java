package Parsers;
/*
 N-Order Markov Assumption
 Tag depends on previous N-tags

 p(Yi|Yi-n,Yi-n+(n-1),.....)
 */

import HMM.Node;

import java.util.List;

/**
 * Abstract class for all n-gram parsers to follow
 */
public abstract class NGram {

    protected int n_order;
    protected List<String> lst_Sentences;
    protected List<Node> lst_Nodes;

    public NGram(int n_order, List<String> lst_Sentences, List<Node> lst_Nodes) {
        this.n_order = n_order;
        this.lst_Sentences = lst_Sentences;
        this.lst_Nodes = lst_Nodes;
    }

    public List<Node> calculateTransitionProbabilities(boolean GoodTuringSmoothing){return null;};
    public void setTransitionProbabilities(){};
}
