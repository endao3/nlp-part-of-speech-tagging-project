package Parsers;
/*
 Second-Order Markov Assumption
 Tag depends on previous two tags

 p(Yi|Yi-2,Yi-1)
 */

import HMM.Node;

import java.util.List;

public class Trigram extends NGram{

    int counter = 0;

    public Trigram(List<String> lst_Sentences, List<Node> lst_Nodes) {
        super(3, lst_Sentences, lst_Nodes);
    }

    /**
     * TODO
     * P(ti|ti-1) for every state
     * Determine if state is eligible initial/final state
     */
    public List<Node> calculateTransitionProbabilities()
    {
        String currentTagValue, nextTagValue_1, nextTagValue_2;
        String currentLine,nextLine_1, nextLine_2;
        String currentWords[], nextWords_1[], nextWords_2[];
        boolean transitionFound;

        //Go through text file
        for (int i = 0; i< lst_Sentences.size(); i++) {
            currentLine = lst_Sentences.get(i);

            if(!currentLine.startsWith("#") && !currentLine.isEmpty()) {
                currentWords = currentLine.split("\t");

                //Go through all nodes in FSA - Find node with tag
                for (Node currentNode : lst_Nodes) {
                    currentTagValue = currentWords[3];
                    /** Set to UNK Tag if applicable*/
                    if(!currentWords[2].matches("[^_=+@#\\\\|\\[{\\]}~&()*'\",<>/]*"))
                        currentTagValue = "X";

                    if(currentTagValue.equals(currentNode.getTagValue()))
                    {
                        //Check if Node can be Initial Node for FSA
                        if (currentWords[0].equals("1")) {

                            //Trigram with two <START> Tags
                            currentNode.setInitialNode(true);
                            lst_Nodes.get(0).addTransition(lst_Nodes.get(0), currentNode, true);
                            counter++;

                            //Trigam with single <START> Tag
                            nextLine_1 = lst_Sentences.get(i+1);
                            if (!nextLine_1.isEmpty()) {
                                nextWords_1 = nextLine_1.split("\t");

                                for (Node nextNode_1 : lst_Nodes) {
                                    nextTagValue_1 = nextWords_1[3];
                                    /** Set to UNK Tag if applicable*/
                                    if(!nextWords_1[2].matches("[^_=+@#\\\\|\\[{\\]}~&()*'\",<>/]*"))
                                        nextTagValue_1 = "X";

                                    if (nextTagValue_1.equals(nextNode_1.getTagValue())) {
                                        lst_Nodes.get(0).addTransition(currentNode, nextNode_1, true);
                                        break;
                                    }
                                }
                            }
                        }

                        //Determine the following tag
                        nextLine_1 = lst_Sentences.get(i + 1);
                        if (!nextLine_1.isEmpty()) {
                            //Determine the subsequent following tag (Second Order/Trigram)
                            nextWords_1 = nextLine_1.split("\t");
                            nextLine_2 = lst_Sentences.get(i+2);
                            nextTagValue_1 = nextWords_1[3];
                            /** Set to UNK Tag if applicable*/
                            if(!nextWords_1[2].matches("[^_=+@#\\\\|\\[{\\]}~&()*'\",<>/]*"))
                                nextTagValue_1 = "X";

                            if(!nextLine_2.isEmpty()) { //Not at end of Trigrams in Sentence
                                nextWords_2 = nextLine_2.split("\t");
                                nextTagValue_2 = nextWords_2[3];
                                /** Set to UNK Tag if applicable*/
                                if(!nextWords_2[2].matches("[^_=+@#\\\\|\\[{\\]}~&()*'\",<>/]*"))
                                    nextTagValue_2 = "X";

                                /**
                                 * P(ti| ti−2ti−1) = C(ti−2ti−1ti) 'Count Trigrams'/ C(ti−2ti−1) 'Count Bigrams'
                                 */
                                transitionFound = false;
                                //Find transition based on following tag
                                for (Node nextNode_1 : lst_Nodes) {
                                    if (nextTagValue_1.equals(nextNode_1.getTagValue())) {
                                        //Find transition based on subsequent following tag
                                        for (Node nextNode_2 : lst_Nodes) {
                                            if (nextTagValue_2.equals(nextNode_2.getTagValue())) {
                                                //Create new Trigram transition
                                                currentNode.addTransition(nextNode_1, nextNode_2, true);
                                                counter++;
                                                transitionFound = true;
                                                break;
                                            }
                                        }
                                    }
                                    if(transitionFound)
                                        break;
                                }
                            }else { //Following Node can be Closure Node for FSA
                                //Find transition based on following tag
                                for (Node nextNode_1 : lst_Nodes) {
                                    if (nextTagValue_1.equals(nextNode_1.getTagValue())) {
                                        nextNode_1.setClosureNode(true);
                                        currentNode.addTransition(nextNode_1, lst_Nodes.get(lst_Nodes.size() - 1), true); //Add transition with <STOP>
                                        counter++;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //Remove all empty connections to other nodes: Add one connection for every node
        for(Node currentNode: lst_Nodes) {
            for (Node newNode1 : lst_Nodes) {
                for (Node newNode2 : lst_Nodes) {
                    currentNode.addTransition(newNode1,newNode2, true);
                }
            }
        }

        setTransitionProbabilities();
        System.out.println("Trigrams:" + counter);
        return lst_Nodes;

    }

    /**
     * P(ti| ti−2ti−1) = C(ti−2ti−1ti) 'Count Trigrams'/ C(ti−2ti−1) 'Count Bigrams'
     * ==>
     * C(ti| ti−2ti−1) / C(ti-1| ti-2)
     */
    public void setTransitionProbabilities(boolean GoodTuringSmoothing)
    {
        for(Node node1: lst_Nodes) {
            node1.setTransitionProbability(node1.getTag().getFrequency(), GoodTuringSmoothing,true);
        }
    }


    /**
     * Return Trigram Transition details
     */
    public String printTransitionProbabilities()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("\nTrigram Transition Probabilities\n" +
                "*******************************");

        for(Node node: lst_Nodes) {
            builder.append(node.toString() + "\n");
            for(Node node2: lst_Nodes) {
                builder.append("\n");
                for(Node node3: lst_Nodes)
                    builder.append(node.printTransitionProbability(node2, node3) + "\n");
            }
        }

        return builder.toString();
    }

}
