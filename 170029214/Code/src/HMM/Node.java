package HMM;
/*Node of Finite State Automaton
        1.Markov Process
        2.Markov Chain
        3.Hidden Markov Model


        TODO - 'q' Parameter
        Probabilities of tag given previous tag sequence/s
        Chain Rule
        Product of
        ----------

        Start of tag sequence:  -1 == 0 = "START"
        End of tag sequence:    n+1 = "STOP"
        i = 1

        Recursive Trigram: q(yi|yi-2, yi-1)

        ==> p(y1|START,START) * p(y2|START,y1) * p(y3|y1,y2).....p(STOP|yn-1, yn)

        TODO - 'e' Parameter
        Probabilities of word given tag
        Chain Rule
        Product of
        ----------

        i = 1

        Recursive: e(xi|yi)

        ==> p(the|DT) * p(NN|tree) * p(VB|fell)
        */

import Smoothing.InterpolatedDiscounting;
import Smoothing.Normalizer;
import Probabilities.Transition;
import Smoothing.SimpleGoodTuring;

import java.util.ArrayList;
import java.util.List;

public class Node {

    private int id;
    private Tag tag;
    private boolean initialNode, closureNode;
    private List<Transition> lst_BigramTransitions; //Collection of transitions with Probabilities
    private List<Transition> lst_TrigramTransitions; //Collection of transitions with Probabilities

    public Node(int id, Tag tag) {
        this.id = id;
        this.tag = tag;

        lst_BigramTransitions = new ArrayList<>();
        lst_TrigramTransitions = new ArrayList<>();
    }

    /**
     * Node details
     * @return
     */
    public int getId() {
        return id;
    }

    public boolean getInitialNode() {
        return initialNode;
    }
    public void setInitialNode(boolean initialNode) {
        this.initialNode = initialNode;
    }

    public boolean getClosureNode() {
        return closureNode;
    }
    public void setClosureNode(boolean closureNode) {
        this.closureNode = closureNode;
    }


    /**
     * Tag Details
     * @return
     */
    public Tag getTag() { return tag; }
    public String getTagValue(){ return tag.getValue(); }


    /**
     * N-Gram Lists
     * @return
     */
    public List<Transition> getBigramTransitions() { return lst_BigramTransitions; }
    public List<Transition> getTrigramTransitions() { return lst_TrigramTransitions; }

    /**
     * --------------------------------------
     * N-GRAMS
     * --------------------------------------
     */

    /**
     * Add new Bigram/Trigram transitions to Node
     * @param node1, node2
     */
    public void addTransition(Node node1, Node node2, boolean typeTrigram){
        //Trigram vs Bigram check
        List<Transition> lst_NGramTransitions = lst_BigramTransitions;
        if(typeTrigram)
            lst_NGramTransitions = lst_TrigramTransitions;


        //Search through list of transitions to node: If found -> Increment: Else -> Create new transition
        boolean found = false;
        for(Transition tr: lst_NGramTransitions)
        {
            if(tr.getFirstNode().compareTo(node1))
            {
                if(typeTrigram) { //Trigram traversal
                    if (tr.getSecondNode().compareTo(node2)) {
                        //Trigram -> Increment transition
                        tr.incrementTransitions();
                        found = true;
                        break;
                    }
                }
                else{
                    //Bigram -> Increment transition
                    tr.incrementTransitions();
                    found = true;
                    break;
                }
            }
        }

        //Add new transition to respective list
        if(!found) {
            if(typeTrigram)
                lst_TrigramTransitions.add(new Transition(node1, node2));
            else
                lst_BigramTransitions.add(new Transition(node1));
        }
    }


    /**
     * Set overall probability of each transition probability from node
     * @param totalNumberOfTags
     */
    public void setTransitionProbability(int totalNumberOfTags, boolean GoodTuringSmoothing, boolean typeTrigram){
        //Trigram vs Bigram check
        List<Transition> lst_NGramTransitions = lst_BigramTransitions;
        if(typeTrigram)
            lst_NGramTransitions = lst_TrigramTransitions;

        if(GoodTuringSmoothing)
            perform_GoodTuringSmoothing(lst_NGramTransitions);
        else
            perform_InterpolatedDiscountingSmoothing(lst_NGramTransitions, totalNumberOfTags, typeTrigram);
    }

    /**
     * Good Turing Smoothing
     * ---------------------
     * Set values per transition
     */
    private void perform_GoodTuringSmoothing(List<Transition> lst_NGramTransitions){
        SimpleGoodTuring sgt = new SimpleGoodTuring();
        sgt.setTransition_Probabilities(this, lst_NGramTransitions);
    }

    /**
     * Perform Kneser-Kay Smoothing
     * Set initial Interpolated Transition Probabilities
     * Account for Tags not found in FSA
     */
    private void perform_InterpolatedDiscountingSmoothing(List<Transition> lst_NGramTransitions, int totalNumberOfTags, boolean typeTrigram)
    {
        double[] normalizedProbabilities = new double[lst_NGramTransitions.size()];
        int i=0, zeroValues = 0;
        double sum=0;

        for(Transition tr: lst_NGramTransitions) {
            if (tr.getNumTransitions() == 1)
                zeroValues++; //Count of missing transition links
        }

        /*IPD Object to call smoothing method*/
        InterpolatedDiscounting ipd = new InterpolatedDiscounting();

        for(Transition tr: lst_NGramTransitions) {
            ipd.setTransition_Probability(tr, totalNumberOfTags, zeroValues, typeTrigram); //IPD Smoothing
            normalizedProbabilities[i] = tr.getTransition_Probability(); //Normalizing
            sum += tr.getTransition_Probability();
            i++;
        }

        /**
         * Reset normalized probabilities to all tags
         */
        if(totalNumberOfTags > 0) {
            normalizedProbabilities = Normalizer.getNormalized_TransitionValues(normalizedProbabilities, sum);

            i = 0;
            for (Transition tr : lst_NGramTransitions) {
                tr.setTransition_Probability(normalizedProbabilities[i]);
                i++;
            }
        }
    }

    /**
     * --------------------------------------
     * PRINT BIGRAM TRANSITION TABLES
     * --------------------------------------
     * @param node
     * @return
     */
    public String printTransitionProbability(Node node) {
        for(Transition tr: lst_BigramTransitions)
        {
            if(tr.getFirstNode().compareTo(node))
                return "Transitions "+ tr.getNumTransitions()+
                        ": "+ node.getTagValue()+
                        "|"+ this.getTagValue()+
                        ": "+ tr.getTransition_Probability();
        }
        return "No connection :" + node.getTagValue()+
                "|"+ this.getTagValue();
    }


    /**
     * --------------------------------------
     * PRINT TRIGRAM TRANSITION TABLES
     * --------------------------------------
     * @param node1, node2
     * @return
     */
    public String printTransitionProbability(Node node1, Node node2) {
        for(Transition tr: lst_TrigramTransitions)
        {
            if(tr.getFirstNode().compareTo(node1) && tr.getSecondNode().compareTo(node2))
                return "Transitions "+ tr.getNumTransitions()+
                        ": "+ node2.getTagValue()+
                        "|"+ this.getTagValue()+
                        ","+ node1.getTagValue()+
                        ": "+ tr.getTransition_Probability();
        }
        return "No connection :" + node2.getTagValue() +
                "|" + this.getTagValue() +
                ","+ node1.getTagValue();
    }


    /**
     * --------------------------------------
     * GENERAL
     * --------------------------------------
     */


    /**
     * Node details
     * @return
     */
    public String toString()
    {
        return("\nNode "+this.getId()+
                ": Tag "+this.getTagValue()+
                ": Initial "+ getInitialNode()+
                ": Closure "+ getClosureNode());
    }

    /**
     * Node1 compare Node2
     * @param node
     * @return
     */
    public boolean compareTo(Node node)
    {
        if(this.getId() == node.getId())
            return true;
        return false;
    }

}
