package HMM;

public class Word_Morphology {

    private String stem;
    private String prefix;
    private String suffix;
    private boolean capital;
    private boolean digit;
    private boolean symbol;
    private int length;

    public Word_Morphology(String word){
    //public Word_Morphology(String word, String stem) {
        prefix = "";
        suffix = "";

        //this.stem = stem;
        //setSuffix(word);//Stemmer

        setisCapital(word);
        setisDigit(word);
        setIsSymbol(word);
        this.length = word.length();

        if(!digit&&!symbol)
            setAffixes(word);//Own algorithm
    }


    /**
     * Set prefix and suffix for word
     * @param word
     */
    private void setAffixes(String word)
    {
        int size = word.length();
        setPrefix(word, size);
        setSuffix(word, size);
    }

    /**
     * Set suffix based on values (length 4 limit)
     * Suffix length:
     *      4 letters for words >= 10 in size
     *      3 letters for words >= 5 in size
     *      2 letters for words ==5 or ==6
     * @param word
     */
    private void setPrefix(String word, int size){
        if(size >=10)
            prefix = word.substring(0,4).toLowerCase();

        if(size >=7 && size < 10)
            prefix = word.substring(0,3).toLowerCase();

        if(size ==5 || size ==6)
            prefix = word.substring(0,2).toLowerCase();
    }

    /**
     * Set suffix based on values (length 4 limit)
     * Suffix length:
     *      4 letters for words >= 10 in size
     *      3 letters for words >= 7 in size
     *      2 letters for words >= 3 in size
     *      1 letters for words == 2 in size
     * @param word
     */
    private void setSuffix(String word, int size){
        if(size >= 10)
            suffix = word.substring(size-4).toLowerCase();

        if(size >=7 && size <10)
            suffix = word.substring(size-3).toLowerCase();

        if(size >= 3 && size < 7)
            suffix = word.substring(size-2).toLowerCase();

        if(size == 2)
            suffix = word.substring(size-1).toLowerCase(); //Last letter
    }


    /**
     * Return prfexi value for word
     * @return
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * Morphological Attributes
     * @return
     */
    public String getSuffix() {
        return suffix;
    }

    /*
    Stemmer suffix
     */
    private void setSuffix(String word) {

        Character c1;
        boolean noChange = (word.compareTo(stem)==0);

        if(!noChange) {
            //Add letters from stem to suffix
            this.suffix = "";
            for (int i = stem.length(); i < word.length(); i++) {
                c1 = word.charAt(i);
                    suffix += (c1);
            }
        }
    }

    public void setisCapital(String word){
        if(Character.isUpperCase(word.charAt(0)))
            this.capital = true;
    }

    public boolean isCapital(){
        return capital;
    }

    /**
     * If Tag contains a symbol and not a digit: X_Symbol == True
     * @param word
     */
    private void setIsSymbol(String word){
        if(!word.matches("[^_=+@#\\\\|\\[{\\]}~&()*'\"<>/]*")) {
            this.symbol = true;
            this.capital = false;
        }
    }

    public boolean isSymbol(){
        return symbol;
    }

    /**
     * Set all Tags with a number: Digit == True
     * @param word
     */
    public void setisDigit(String word){
        for (char c : word.toCharArray()) {
            if (Character.isDigit(c)) {
                this.digit = true;
                this.symbol = false;
                this.capital = false;
            }
            /*if(Character.isLetter(c)) {
                this.digit = false;
                break;
            }*/
        }
    }

    public boolean isDigit(){
        return digit;
    }

    public int getLength() { return length; }
}
