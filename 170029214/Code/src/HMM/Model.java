package HMM;

import ML_Algorithms.*;
import Parsers.Bigram;
import Parsers.NGram;
import Parsers.Trigram;
import Smoothing.InterpolatedDiscounting;
import Smoothing.SimpleGoodTuring;
import Smoothing.SmoothedProbability;
import Strings.TagStrings;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.List;

public class Model {

    public String language;
    private boolean GoodTuringSmoothing;

    private List<String> listOfSentences;
    private List <Word> listOfWords;
    private List <Tag> listOfTags;
    private List <Node> listOfNodes;

    private DecisionTree decisionTree;
    private Map<String,List<SmoothedProbability>> tbl_GTValues;
    private Map<String,List<SmoothedProbability>> tbl_IPDValues;

    private StringBuilder probabilitiesBuilder;
    private Stemmer stemmer;

    private int numberOfSentences = 0;
    private int totalWords_TrainingSet;

    /**
     * Constructor
     */
    public Model() {
       probabilitiesBuilder = new StringBuilder();
    }

    /**
     * Setup and train HMM
     * @param trainingFile
     */
    public void setupHMM(File trainingFile)
    {
        System.out.println("Setting up Finite State Machine...>>!!");
        listOfTags = setListOfTags(true);
        readFile(trainingFile);
        captureWords();
        setupFiniteStateMachine();

        //Setup Stemmer and Calculate Probabilities
        stemmer = setupStemmer(trainingFile.getName());

        calculateProbabilities();
        generate_GTTables();
    }

    /**
     * Reset all model parameters
     */
    public void resetParameters(){

        listOfSentences.clear();
        listOfWords.clear();
        listOfTags.clear();
        listOfNodes.clear();

        if(GoodTuringSmoothing)
            tbl_GTValues.clear();
        else
            tbl_IPDValues.clear();

        probabilitiesBuilder = new StringBuilder();
        numberOfSentences = 0;
        totalWords_TrainingSet=0;
    }


    /**
     * Setup Stemmer based on language type
     * @param fileName
     * @return
     */
    private Stemmer setupStemmer(String fileName){
        String dialect = (fileName.substring(0,fileName.indexOf("_")).toLowerCase());
        return new Stemmer(dialect);
    }


    /**
     * Set list of tags for corpus
     */
    static List<Tag> setListOfTags(boolean UD_TagSet)
    {
        if(!UD_TagSet)
            return TagStrings.getPennTagSet();

        return TagStrings.getUDTagSet();
    }


    /**
     * Read input as sentences from file into List<String>
     * @param file
     */
    public void readFile(File file)
    {
        try
        {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
            String line;
            listOfSentences = new ArrayList<>();

            while ((line = reader.readLine()) != null) {
                listOfSentences.add(line);
            }
            reader.close();
        }
        catch (Exception e)
        {
            System.err.format("Exception occurred trying to read '%s'.", file);
            e.printStackTrace();
        }
    }


    /**
     * Get all words from List of sentences
     */
    private void captureWords()
    {
        boolean wordFound = false;
        String currentWord, words [], setTag="";
        listOfWords = new ArrayList<>();;

        for(String line: listOfSentences)
        {
            if(line.startsWith("# text ="))
                numberOfSentences++;

            wordFound = false;
            if(!line.startsWith("#") && !line.isEmpty())
            {
                totalWords_TrainingSet++;
                words = line.split("\t");

                currentWord = words[1];//Word
                //currentWord = words[2];//Lemma

                setTag = words[3];//Universal tag
                //setTag = words[4];//Language specific tag
                //setTag = TagStrings.checkTag(setTag);

                /** Set to SYM or NUM Tag if applicable*/
                if(!currentWord.matches("[^£$€%^_=+@#|~&*<>]*")) {
                    setTag = "SYM";
                }

                for (char c : currentWord.toCharArray()) {
                    if (Character.isDigit(c)) {
                        setTag = "NUM"; //CD
                        break;
                    }
                }


                if(!listOfWords.isEmpty()) {
                    for (Word word : listOfWords) {

                        // If word and tag is the same as previous -> Increment word and tag frequency
                        if (word.compareTo(word.getValue(), currentWord) && word.compareTo(word.getTag().getValue(), setTag)) {
                            word.incrementFrequency();

                            for(Tag tag: listOfTags) {
                                if(tag.getValue().equals(setTag)) {
                                    tag.incrementFrequency();
                                    break;
                                }
                            }

                            wordFound = true;
                            break;
                        }
                    }
                }

                // If no match was found, add new word to list
                if(!wordFound) {
                    for(Tag tag: listOfTags) {
                        if(tag.getValue().equals(setTag)) {
                            listOfWords.add(new Word(currentWord, tag));
                            tag.incrementFrequency();
                            break;
                        }
                    }
                }

            }
        }

        // Number of sentences = number of tags for <START> and <STOP>
        listOfTags.get(0).setFrequency(numberOfSentences);//<START> Tag
        listOfTags.get(listOfTags.size()-1).setFrequency(numberOfSentences);//<STOP> Tag
    }


    /**
     * Create a node with a state for every number of tags
     */
    private void setupFiniteStateMachine()
    {
        int id = -1;
        int totalTags = 0;
        listOfNodes = new ArrayList<>();

        probabilitiesBuilder.append("\n");
        //Add new nodes to FSA
        for(Tag tag: listOfTags)
        {
            listOfNodes.add(new Node(++id, tag));

            probabilitiesBuilder.append(listOfNodes.get(id).getId() + ": "
                    + listOfNodes.get(id).getTag().toString() +"\n");

            totalTags += tag.getFrequency();
        }

        probabilitiesBuilder.append("Tags (Inclusive of <START> and <STOP>): "+ totalTags + "\n\n");

        totalTags -= listOfNodes.get(0).getTag().getFrequency()*2; //<START> and <STOP> tags
        for(Tag tag: listOfTags) {
            tag.setOverallProbability(totalTags);
        }
    }

    /**
     * Calculate probabilities
     */
    public void calculateProbabilities()
    {
        System.out.println("\n\n*******************************\nPlease choose Smoothing Style");
        Scanner in = new Scanner(System.in);
        boolean choiceMade = false;

        try {
            while (!choiceMade) {
                System.out.println(
                        "*******************************" +
                        "\n1. Good Turing" +
                        "\n2. Interpolated Discounting" +
                        "\nOption: ");
                int choice = in.nextInt();

                switch (choice) {
                    case 1:
                        GoodTuringSmoothing = true;
                        choiceMade = true;
                        break;
                    case 2:
                        GoodTuringSmoothing = false;
                        choiceMade = true;
                        break;
                    default:
                        System.out.println("\n\nERROR: Input error.\n Please enter 1 or 2!!!\n\n");
                }
            }
        }catch(Exception e){
            System.out.println("\n\nInput Mismatch\nPlease Enter Integer values only!!!\n\n");
            calculateProbabilities();
        }

        calculateBigramTransitionProbabilities(); //Transmission probability for each state (Tag | Tag)
        //calculateTrigramTransitionProbabilities(); //Transmission probability for each state (Tag | Tag,Tag)
        calculateEmissionProbabilities(); //probability of Word->Tag
    }



    /**
     * P(ti|ti-1) for every state
     * Determine if state is eligible initial/final state
     */
    private void calculateBigramTransitionProbabilities()
    {
        NGram bigramModel = new Bigram(listOfSentences,listOfNodes);
        listOfNodes = bigramModel.calculateTransitionProbabilities(GoodTuringSmoothing);
        probabilitiesBuilder.append(((Bigram)bigramModel).printTransitionProbabilities());
    }


    /**
     * P(ti|ti-2,ti-1) for every state
     * Determine if state is eligible initial/final state
     */
    private void calculateTrigramTransitionProbabilities()
    {
        NGram trigramModel = new Trigram(listOfSentences,listOfNodes);
        listOfNodes = trigramModel.calculateTransitionProbabilities(GoodTuringSmoothing);
        probabilitiesBuilder.append(((Trigram)trigramModel).printTransitionProbabilities());
    }

    /**
     * Emission calculated per word
     */
    private void calculateEmissionProbabilities()
    {
        int unknownWord_CapitalFrequency = 0;
        //decisionTree = new DecisionTree(listOfTags, listOfWords);

        Collections.sort(listOfWords, new SortWordByBestValue());
        probabilitiesBuilder.append("\nEmission Probabilities\n" +
                "**********************\n");

        for(Word word: listOfWords) {

            if(word.getFrequency() == 1) {
                /** Pass in X Tag for potential SYM and NUM affixes */
                word.setMorphology(listOfTags.get(listOfTags.size()-2));//, stemmer.getStem(word.getValue()));

                if(word.getMorphology().isCapital()) {
                    word.getTag().incrementCapitalFrequency();
                    unknownWord_CapitalFrequency++;
                }
            }

            word.setEmissionValue();
            probabilitiesBuilder.append(word);
        }

        setCapitalProbabilities(unknownWord_CapitalFrequency);

        probabilitiesBuilder.append("\n");
        probabilitiesBuilder.append("\nTotal Words: " + totalWords_TrainingSet);
        probabilitiesBuilder.append("\nWord List size: " + listOfWords.size());
    }


    /**
     * Good-Turing Tables
     * Get Count:Frequency for each Tag using word counts
     */
    private void generate_GTTables() {
        if(GoodTuringSmoothing) {
            SimpleGoodTuring sgt = new SimpleGoodTuring();
            tbl_GTValues = sgt.setEmission_GTTables(listOfTags, listOfWords);
        }

        else {
            InterpolatedDiscounting ipd = new InterpolatedDiscounting();
            tbl_IPDValues = ipd.setEmission_IPDTables(listOfTags, listOfWords);
        }


        System.out.println("Smoothing complete\n");
    }

    /**
     * Set capital probabilities per tag
     */
    private void setCapitalProbabilities(int unknownWord_CapitalFrequency){
        for(Tag tag: listOfTags){
            tag.setCapitalProbability(unknownWord_CapitalFrequency);
        }
    }



    /**
     * --------------
     * GETTER METHODS
     * --------------
     */

    public String getLanguage() {
        return language;
    }

    public List<String> getListOfSentences() {
        return listOfSentences;
    }

    public List<Word> getListOfWords() {
        return listOfWords;
    }

    public List<Tag> getListOfTags() {
        return listOfTags;
    }

    public List<Node> getListOfNodes() {
        return listOfNodes;
    }

    public DecisionTree getDecisionTree() {
        return decisionTree;
    }

    public Map<String, List<SmoothedProbability>> getTbl_GTValues() {
        return tbl_GTValues;
    }

    public Map<String, List<SmoothedProbability>> getTbl_IPDValues() { return tbl_IPDValues; }

    public StringBuilder getProbabilitiesBuilder() {
        return probabilitiesBuilder;
    }

    public boolean isGoodTuringSmoothing() {
        return GoodTuringSmoothing;
    }


    public Stemmer getStemmer() {
        return stemmer;
    }
}



/**
 * Sort by alphabet
 */
class SortWordByBestValue implements Comparator<Word>
{
    @Override
    public int compare(Word n1, Word n2)
    {
        if(n1.getValue().compareTo(n2.getValue()) < 0)
            return -1;
        else if(n1.getValue().compareTo(n2.getValue()) > 0)
            return 1;
        return 0;
    }
}