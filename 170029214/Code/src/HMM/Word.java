package HMM;

public class Word {

    private String value;
    private int frequency;
    private double emissionProbability;

    private Tag tag;
    private Word_Morphology morphology;

    /**
     * Basic Word constructor
     * No Tag required
     * @param value
     */
    public Word(String value) {
        this.value = value;
    }

    /**
     * Constrcutor with Tag and Value
     * @param value
     * @param tag
     */
    public Word(String value, Tag tag){
        this.value = value;
        this.tag = tag;
        frequency = 1;
    }

    public String getValue() { return this.value; }

    /**
     * Word frequency details
     */
    public void incrementFrequency()
    {
        this.frequency++;
    }
    public int getFrequency()
    {
        return frequency;
    }

    /**
     * Tag Details
     * @return
     */
    public Tag getTag(){ return this.tag; }
    public void setTag(Tag tag) {
        this.tag = tag;
    }

    /**
     * Word Emission details
     * @return
     */
    public double getEmission() {
        return emissionProbability;
    }

    public void setEmissionValue(){
        emissionProbability = (double)this.getFrequency()/this.getTag().getFrequency();
    }

    /**
     * Word Morphology
     */
    public void setMorphology(Tag xTag){
    //public void setMorphology(Tag xTag, String stem){
        morphology = new Word_Morphology(this.value);
        //morphology = new Word_Morphology(this.value, stem);
        if(morphology.getSuffix()!=null || morphology.getPrefix()!=null)
        {
            if(this.tag.getValue().equals("SYM") || this.tag.getValue().equals("NUM"))
                xTag.addWord_Morphologies(this.morphology);
            else
                this.tag.addWord_Morphologies(this.morphology);
        }

    }

    public Word_Morphology getMorphology() {
        return morphology;
    }

    /**
     * Return String details of Word object
     * @return
     */
    public String toString(){
        return getValue() +
                " - " + frequency +
                " - " + tag.getValue() +
                " - " + emissionProbability + "\n";
    }

    /**
     * Compare two words
     * @param w1
     * @param w2
     * @return
     */
    public boolean compareTo(String w1, String w2)
    {
        if(w1.equals(w2))
            return true;
        return false;
    }

}


