package HMM;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Tag {

    private String value;
    private int frequency;
    private int capitalFrequency;
    private double overallProbability, capitalProbability;
    private Map<String, Integer> map_Suffix;

    public Tag(String value){
        this.value = value;
        this.frequency = 0;
        this.capitalFrequency = 0;
        map_Suffix = new HashMap<>();
    }

    public String getValue(){
        return value;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency){
        this.frequency = frequency;
    }

    public void incrementFrequency() {
        this.frequency++;
    }

    public void setOverallProbability(int totalTags){ this.overallProbability = (double)this.frequency/totalTags; }

    public double getOverallProbability(){return this.overallProbability;}

    /**
     * Add specific affix to correct Affix List
     * ----------------------------------------
     * If affix is new, add to list for Affix list
     * Else increment counter for affix in required list
     *
     * @param morphology
     */
    public void addWord_Morphologies(Word_Morphology morphology) {
        if(!morphology.getSuffix().equals("")) {
            Integer freq_1 = map_Suffix.get(morphology.getSuffix());
            if (freq_1 == null)
                map_Suffix.put(morphology.getSuffix(), 1);
            else
                map_Suffix.put(morphology.getSuffix(), freq_1 + 1);
        }
    }

    /**
     * Check list of word_morphologies to find affix
     * Return to Normalizer class - getNormalized_AffixValue() method
     */
    public int getAffixFrequency(String affix){
        if(map_Suffix.containsKey(affix) && !affix.equals(""))
            return map_Suffix.get(affix);

        return 0;
    }


    /**
     * Capital Probability methods
     */
    public void incrementCapitalFrequency(){
        this.capitalFrequency++;
    }

    public double getCapitalProbability() { return capitalProbability; }

    public void setCapitalProbability(int totalCapitals){
        this.capitalProbability = (double)this.capitalFrequency/totalCapitals;
    }



    /**
     * Return tag details
     * @return
     */
    public String toString() {
        return this.getValue() + ":"+this.getFrequency();
    }
}