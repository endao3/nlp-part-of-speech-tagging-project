
import java.awt.*;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.io.*;

import HMM.*;
import IO_Operations.WriteOutput;
import ML_Algorithms.*;

public class Runner {

    static String currentDirectory = (System.getProperty("user.dir"));

    static Model model;
    public static String language;
    private static File trainingFile, testingFile;

    public static void main(String [] args) throws Exception
    {
        System.out.println("Starting...");
        setFiles();
        setupProgram();
        runProgram();

        printMenu();
        System.exit(0);
    }


    /**
     * Select training and testing files
     * @return
     */
    public static void setFiles(){
        /**
         * FILES
         */
        System.out.println("\nSELECT TRAINING CORPUS");
        trainingFile = new File(addCorpus());
        System.out.println(trainingFile);

        System.out.println("\nSELECT Testing CORPUS");
        testingFile = new File(addCorpus());
        System.out.println(testingFile+"\n");
    }


    /**
     * Select conllu corpus for HMM
     * @return
     */
    public static String addCorpus(){
        String file = null;

        FileDialog dialog = new FileDialog((Frame)null, "Select \"conllu\" File to Open");
        dialog.setDirectory(currentDirectory);
        String extension = "";

        while(file == null || file.isEmpty() || !extension.equals(".conllu")) {
            dialog.setMode(FileDialog.LOAD);
            dialog.setVisible(true);
            currentDirectory = dialog.getDirectory();
            file = dialog.getFile();
            if(file != null && !file.isEmpty())
                extension = file.substring(file.indexOf("."));
        }
        return dialog.getDirectory() + file;
    }


    /**
     *
     *1 SETUP HMM
     *2.Write probabilities(Pre-Smoothing) to File
     */

    public static void setupProgram(){

        model = new Model();
        model.setupHMM(trainingFile);

        WriteOutput writer = new WriteOutput();
        try {
            writer.writeOutput("Probability_Outputs", model.getProbabilitiesBuilder());
        }catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * START BIGRAM VITERBI
     */
    public static void runProgram(){
        try {
            startBigramViterbi(testingFile);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    /**
     * Menu for program
     */
    private static void printMenu(){
        Scanner in = new Scanner(System.in);
        boolean choiceMade = false, inputError = false;

        System.out.println("\n\n***********\nSystem Menu\n***********\n");

        try {
            while(!choiceMade) {
                System.out.print("\nPlease choose an option" +
                        "\n-----------------------" +
                        "\n1. Repeat " + testingFile +
                        "\n2. Select new corpus" +
                        "\n3. Exit program" +
                        "\nOption: ");

                int choice = in.nextInt();

                switch (choice) {
                    case 1:
                        runProgram();
                        break;
                    case 2:
                        model.resetParameters();
                        setFiles();
                        setupProgram();
                        runProgram();
                        System.out.println("Viterbi Finished.\n********************\n\n\n");
                        break;
                    case 3:
                        choiceMade = true;
                        in.close();
                        System.out.println("\n\n****************\nExited Program.\n****************\n");
                        break;
                    default:
                        System.out.println("\n\nERROR: Input error.\n Please enter 1, 2 or 3!!!\n\n");
                }
            }
        } catch (InputMismatchException e) {
            System.out.println("\n\nInput Mismatch\nPlease Enter Integer values only!!!\n\n");
            printMenu();
        }
    }



    /**
     * Run Bigram Viterbi program for files selected
     * Determine tag sequences
     * Determine overall accuracy
     * Time total execution
     * @param testingFile
     * @throws Exception
     */
    static void startBigramViterbi(File testingFile)throws Exception
    {
        model.readFile(testingFile);

        System.out.println("\n--------------------------" +
                "\nBigram Viterbi Starting..." +
                "\n--------------------------\n");
        Viterbi_Bigram viterbi = Viterbi_Bigram.getInstance();
        viterbi.setupViterbi(model);
        Instant start = Instant.now();

        viterbi.getTagSequence();
        viterbi.calculateAccuracy();

        Instant finish = Instant.now();
        long timeElapsed = Duration.between(start, finish).toMinutes();  //in millis
        System.out.println("Time Elapsed:" + timeElapsed+" mins");
    }


    /**
     * Viterbi Trigram - discontinued
     * @param testingFile
     * @throws Exception
     *
    static void startTrigramViterbi(File testingFile)throws Exception
    {
    readFile(testingFile);

    System.out.println("\n---------------------------" +
    "\nTrigram Viterbi Starting..." +
    "\n---------------------------\n");
    Viterbi_Trigram viterbi = Viterbi_Trigram.getInstance();
    viterbi.setupViterbi(model);

    Instant start = Instant.now();

    viterbi.getTagSequence();
    viterbi.calculateAccuracy();

    Instant finish = Instant.now();
    long timeElapsed = Duration.between(start, finish).toMinutes();  //in millis
    System.out.println("Time Elapsed:" + timeElapsed+" mins");
    }*/
}

