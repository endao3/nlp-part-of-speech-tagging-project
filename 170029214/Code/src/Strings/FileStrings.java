package Strings;

public class FileStrings {

    static final String currentDirectory = (System.getProperty("user.dir"));

    //ENGLISH CORPUS
    static final String English_EWT_TrainingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_English-EWT/en_ewt-ud-train.conllu");
    static final String English_EWT_DevelopmentDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_English-EWT/en_ewt-ud-dev.conllu");
    static final String English_EWT_TestingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_English-EWT/en_ewt-ud-test.conllu");

    static final String English_LinES_TrainingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_English-LinES/en_lines-ud-train.conllu");
    static final String English_LinES_DevelopmentDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_English-LinES/en_lines-ud-dev.conllu");
    static final String English_LinES_TestingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_English-LinES/en_lines-ud-test.conllu");

    static final String English_ParTUT_TrainingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_English-ParTUT/en_partut-ud-train.conllu");
    static final String English_ParTUT_DevelopmentDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_English-ParTUT/en_partut-ud-dev.conllu");
    static final String English_ParTUT_TestingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_English-ParTUT/en_partut-ud-test.conllu");

    static final String English_GUM_TrainingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_English-GUM/en_gum-ud-train.conllu");
    static final String English_GUM_DevelopmentDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_English-GUM/en_gum-ud-dev.conllu");
    static final String English_GUM_TestingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_English-GUM/en_gum-ud-test.conllu");


    //IRISH CORPUS
    static final String Irish_IDT_TrainingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_Irish-IDT/ga_idt-ud-train.conllu");
    static final String Irish_IDT_TestingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_Irish-IDT/ga_idt-ud-test.conllu");


    //SPANISH CORPUS
    static final String Spanish_GSD_TrainingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_Spanish-GSD/es_gsd-ud-train.conllu");
    static final String Spanish_GSD_TestingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_Spanish-GSD/es_gsd-ud-test.conllu");

    static final String Spanish_AnCora_TrainingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_Spanish_AnCora/es_ancora-ud-train.conllu");
    static final String Spanish_AnCora_TestingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_Spanish_AnCora/es_ancora-ud-test.conllu");


    //FRENCH CORPUS
    static final String French_GSD_TrainingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_French-GSD/fr_gsd-ud-train.conllu");
    static final String French_GSD_TestingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_French-GSD/fr_gsd-ud-test.conllu");

    static final String French_Spoken_TrainingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_French-Spoken/fr_spoken-ud-train.conllu");
    static final String French_Spoken_TestingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_French-Spoken/fr_spoken-ud-test.conllu");

    static final String French_Sequoia_TrainingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_French-Sequoia/fr_sequoia-ud-train.conllu");
    static final String French_Sequoia_TestingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_French-Sequoia/fr_sequoia-ud-test.conllu");


    //GERMAN CORPUS
    static final String German_GSD_TrainingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_German-GSD/de_gsd-ud-train.conllu");
    static final String German_GSD_TestingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_German-GSD/de_gsd-ud-test.conllu");


    //ITALIAN CORPUS
    static final String Italian_ParTUT_TrainingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_Italian-ParTUT/it_partut-ud-train.conllu");
    static final String Italian_ParTUT_TestingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_Italian-ParTUT/it_partut-ud-test.conllu");

    static final String Italian_PoSTWITA_TrainingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_Italian-PoSTWITA/it_postwita-ud-train.conllu");
    static final String Italian_PoSTWITA_TestingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_Italian-PoSTWITA/it_postwita-ud-test.conllu");


    //DUTCH CORPUS
    static final String Dutch_Alpino_TrainingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_Dutch-Alpino/nl_alpino-ud-train.conllu");
    static final String Dutch_Alpino_TestingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_Dutch-Alpino/nl_alpino-ud-test.conllu");

    static final String Dutch_LassySmall_TrainingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_Dutch-LassySmall/nl_lassysmall-ud-train.conllu");
    static final String Dutch_LassySmall_TestingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_Dutch-LassySmall/nl_lassysmall-ud-test.conllu");


    //PORTUGUESE
    static final String Portuguese_Bosque_TrainingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_Portuguese-Bosque/pt_bosque-ud-train.conllu");
    static final String Portuguese_Bosque_TestingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_Portuguese-Bosque/pt_bosque-ud-test.conllu");


    //RUSSIAN
    static final String Russian_SynTagRus_TrainingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_Russian-SynTagRus/ru_syntagrus-ud-train.conllu");
    static final String Russian_SynTagRus_TestingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_Russian-SynTagRus/ru_syntagrus-ud-test.conllu");

    static final String Russian_Taiga_TrainingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_Russian-Taiga/ru_taiga-ud-train.conllu");
    static final String Russian_Taiga_TestingDataset = (currentDirectory + "/UniversalDependencies/ud-treebanks-v2.3/UD_Russian-Taiga/ru_taiga-ud-test.conllu");

}
