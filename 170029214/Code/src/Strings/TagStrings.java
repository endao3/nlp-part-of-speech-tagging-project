package Strings;

import HMM.Tag;

import java.util.ArrayList;
import java.util.List;

public class TagStrings {

    private static List<Tag> listOfTags;

    /**
     * Set Universal language independent tags
     * @return
     */
    public static List<Tag> getUDTagSet(){
        listOfTags = new ArrayList<>();

        /**
         *     Starter:                      <tag name="<START>"> Starter Tag </tag>
         *     Adjective:                   <tag name="ADJ">15963</tag><!-- good, great, other, best, new, many, more, last, same, few -->
         *     Ad-position:                  <tag name="ADP">21663</tag><!-- of, in, to, for, on, with, at, from, by, as -->
         *     Adverb:                      <tag name="ADV">13045</tag><!-- so, just, when, very, also, how, now, even, there, then -->
         *     Auxiliary Verb:              <tag name="AUX">15389</tag><!-- be, have, will, do, can, would, could, should, may, might -->
         *     Coordinating Conjunction:    <tag name="CCONJ">8226</tag><!-- and, but, or, &, both, either, yet, nor, plus, not -->
         *     Determiner:                  <tag name="DET">20076</tag><!-- the, a, this, all, some, any, no, that, these, another -->
         *     Interjection:                <tag name="INTJ">923</tag><!-- please, yes, well, no, hi, like, ok, lol, hey, oh -->
         *     Noun:                        <tag name="NOUN">43103</tag><!-- time, year, service, place, day, people, thanks, food, way, number -->
         *     Numerical:                   <tag name="NUM">4912</tag><!-- one, two, 2, 3, 5, 1, 10, 4, three, 20 -->
         *     Particle:                    <tag name="PART">6827</tag><!-- to, not, 's -->
         *     Pronoun:                     <tag name="PRON">22955</tag><!-- i, you, it, they, we, he, my, that, she, this -->
         *     Proper Noun:                 <tag name="PROPN">16899</tag><!-- bush, US, al, Iraq, enron, united, Iran, New, China, states -->
         *     Punctuation:                 <tag name="PUNCT">29868</tag><!-- ., ,, ", -, ?, ), (, :, !, ... -->
         *     Subordinating Conjunction:   <tag name="SCONJ">4637</tag><!-- that, if, as, because, for, of, since, before, like, while -->
         *     Symbol:                      <tag name="SYM">765</tag><!-- $, -, :), %, /, +, |, :(, :-), a -->
         *     Verb:                        <tag name="VERB">28437</tag><!-- have, be, go, get, do, know, say, make, take, want -->
         *     Other:                       <tag name="X">1141</tag><!-- etc, 1, 2, etc., 3, a, c​a​r​o​l​.​s​t​.​c​l​a​i​r​@​e​n​r​o​n​.​c​o​m, 4, over, - -->
         *     Stopper:                     <tag name="<Stop>"> Stopper Tag </tag>
         */
        listOfTags.add(new Tag("<START>"));
        listOfTags.add(new Tag("ADJ"));listOfTags.add(new Tag("ADP"));
        listOfTags.add(new Tag("ADV"));listOfTags.add(new Tag("AUX"));
        listOfTags.add(new Tag("CCONJ"));
        listOfTags.add(new Tag("DET"));listOfTags.add(new Tag("INTJ"));
        listOfTags.add(new Tag("NOUN"));listOfTags.add(new Tag("NUM"));
        listOfTags.add(new Tag("PART"));listOfTags.add(new Tag("PRON"));
        listOfTags.add(new Tag("PROPN"));listOfTags.add(new Tag("PUNCT"));
        listOfTags.add(new Tag("SCONJ"));listOfTags.add(new Tag("SYM"));
        listOfTags.add(new Tag("VERB"));listOfTags.add(new Tag("X"));
        listOfTags.add(new Tag("<STOP>"));

        return listOfTags;
    }

    /**
     * Set language dependent tags
     * @return
     */
    public static List<Tag> getPennTagSet(){
        listOfTags = new ArrayList<>();
        /**Number Tag Description
         1.	CC	Coordinating conjunction
         2.	CD	Cardinal number
         3.	DT	Determiner
         4.	EX	Existential there
         5.	FW	Foreign word
         6.	IN	Preposition or subordinating conjunction
         7.	JJ	Adjective
         8.	JJR	Adjective, comparative
         9.	JJS	Adjective, superlative
         10. LS	List item marker
         11.	MD	Modal
         12.	NN	Noun, singular or mass
         13.	NNS	Noun, plural
         14.	NNP	Proper noun, singular
         15.	NNPS	Proper noun, plural
         16.	PDT	Predeterminer
         17.	POS	Possessive ending
         18.	PRP	Personal pronoun
         19.	PRP$	Possessive pronoun
         20.	RB	Adverb
         21.	RBR	Adverb, comparative
         22.	RBS	Adverb, superlative
         23.	RP	Particle
         24.	SYM	Symbol
         25.	TO	to
         26.	UH	Interjection
         27.	VB	Verb, base form
         28.	VBD	Verb, past tense
         29.	VBG	Verb, gerund or present participle
         30.	VBN	Verb, past participle
         31.	VBP	Verb, non-3rd person singular present
         32.	VBZ	Verb, 3rd person singular present
         33.	WDT	Wh-determiner
         34.	WP	Wh-pronoun
         35.	WP$	Possessive wh-pronoun
         36.	WRB	Wh-adverb
         37.    X   Unknown (Not part of original tagset)
         */

        listOfTags.add(new Tag("<START>"));
        listOfTags.add(new Tag("CC"));listOfTags.add(new Tag("$"));
        listOfTags.add(new Tag("CD"));listOfTags.add(new Tag("DT"));
        listOfTags.add(new Tag("EX"));listOfTags.add(new Tag("FW"));
        listOfTags.add(new Tag("IN"));listOfTags.add(new Tag("JJ"));
        listOfTags.add(new Tag("JJR"));listOfTags.add(new Tag("JJS"));
        listOfTags.add(new Tag("LS"));listOfTags.add(new Tag("MD"));
        listOfTags.add(new Tag("NN"));listOfTags.add(new Tag("NNS"));
        listOfTags.add(new Tag("NNP"));listOfTags.add(new Tag("NNPS"));
        listOfTags.add(new Tag("PDT"));listOfTags.add(new Tag("POS"));
        listOfTags.add(new Tag("PRP"));listOfTags.add(new Tag("PRP$"));
        listOfTags.add(new Tag("RB"));listOfTags.add(new Tag("RBR"));
        listOfTags.add(new Tag("RB$"));listOfTags.add(new Tag("RP"));
        listOfTags.add(new Tag("SYM"));listOfTags.add(new Tag("TO"));
        listOfTags.add(new Tag("UH"));listOfTags.add(new Tag("VB"));
        listOfTags.add(new Tag("VBD"));listOfTags.add(new Tag("VBG"));
        listOfTags.add(new Tag("VBN"));listOfTags.add(new Tag("VBP"));
        listOfTags.add(new Tag("VBZ"));listOfTags.add(new Tag("WDT"));
        listOfTags.add(new Tag("WP"));listOfTags.add(new Tag("WP$"));
        listOfTags.add(new Tag("WRB"));listOfTags.add(new Tag("HYPH"));
        listOfTags.add(new Tag("."));
        listOfTags.add(new Tag("X"));
        listOfTags.add(new Tag("<STOP>"));

        return listOfTags;
    }

    /**
     * Check for outlier tags: Set to X if not found in original list
     *
     * Tags
     * ----
     *  "
     *  ,
     *  :
     *  ``
     *
     * @param currentValue
     * @return
     */
    public static String checkTag(String currentValue){
        if(!currentValue.matches("[^_=+@#\\\\|\\[{\\]}~&()`,:*'\",<>/]*")
                || currentValue.equals("-LSB-")
                || currentValue.equals("-RSB-"))
            return "X";
        return currentValue;
    }


}
